# How to use test template to tetsssssst track the datatool creation

*1. Clone this repository locally into your <working_dir> .*
```
git clone <clone_link> <working_dir>
```

*2. Create a repository for new datatool in gitlab and copy the clone link.*

*3. Go to the cloned repo directory and run the setup script.*
```
cd <working_dir>
source setup.sh -l <datatool_repo_clone_link> -p <path_to_directory_where_to_clone_datatool>
```
After running the script, you will have the skeleton ready for your datatool. 

*4. Edit the `source_data_parser.py` script to implement the parsing code for the source dataset. You will have to use the [datatool_api](https://gitlab.com/bonseyes/artifacts/data_tools/datatool-api) in your code to ensure the annotations are validated and added correctly.* 

*5. Edit the `plot_annotations/plot_annotations.py` script to implement the plotting code for the annotations. You can use the [plot_utils](https://gitlab.com/bonseyes/artifacts/data_tools/datatool-common-base/-/tree/master/plot_annotations) in your code to implement the plotting.* 

*6. Once tested locally, build the docker image, test and push it using the instructions from [Docker Page](/docker).*

*7. Edit the README.md with appropriate and add appropriate information for the placeholders marked by {}.*

*8. Commit and push your changes to the datatool gitlab repository.* 
