import os

from tqdm import tqdm
from PIL import Image


def get_corrupt_images(image_dir_path: str, ext_list: list[str] = [".jpg", ".jpeg", ".png"]) -> list[str]:
	"""
	Get list of corrupted images in the specified directory tree.
	
	Parameters
	----------
	image_dir_path : str
		Path of the directory tree containing the images to be checked.
	ext_list : list[str]
		List of extensions of the images to be checked.
	
	Returns
	-------
	corrupt_image_list : list[str]
		List of the paths of the corrupted images.
	"""
	
	if not os.path.exists(image_dir_path):
		raise ValueError("Specified image dir path does not exist.")
	
	corrupt_image_list = []
	
	for root, dirs, files in os.walk(image_dir_path):
		for fname_full in tqdm(files, desc="Scanning tree for corrupt images"):
			fname, fext = os.path.splitext(fname_full)
			if fext in ext_list:
				fpath = os.path.join(root, fname_full)
				try:
					Image.open(fpath).getdata()
				except OSError:
					corrupt_image_list.append(fpath)
	
	return corrupt_image_list
