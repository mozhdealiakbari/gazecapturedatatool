import os
import sys
import argparse
from remote_proxies.proxy_s3 import proxy_s3
from remote_proxies.proxy_synology import proxy_synology
import inspect
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, cur_path + '/../')
from schema_validation.validate_schema import validate_schema


def upload(archive_path, dataset_name, dataset_version, dataset_tag, storage_config, storage_schema):
    parameters = validate_schema(storage_config, storage_schema)
    assert parameters['storage'] in ['synology', 's3'], "storage must be of type synology or s3 to upload"

    package_name = '{}_{}_{}.zip'.format(dataset_name, dataset_version, dataset_tag)
    assert package_name == os.path.basename(archive_path), "archive path must contain the dataset name, version and tag"

    if parameters['storage'] == 'synology':
        client = proxy_synology(parameters['storage_loc'], parameters['storage_root'][parameters['storage']])
    else:
        client = proxy_s3(parameters['storage_root'][parameters['storage']])

    destination_cloud_dir = '{}/{}/{}'.format(dataset_name, dataset_version, dataset_tag)
    client.upload_package(destination_cloud_dir, archive_path)

    # Also upload the meta files
    archive_dir = os.path.dirname(archive_path)
    meta_files = ['info.txt', 'info.yml']
    for mFile in meta_files:
        if os.path.exists(os.path.join(archive_dir, mFile)):
            client.upload_package(destination_cloud_dir, os.path.join(archive_dir, mFile))


def parse_args():
    parser = argparse.ArgumentParser(description="Upload a zip archive to remote storage")
    parser.add_argument('--archive-path', '-p', required=True, help='Path to the archive to be uploaded')
    parser.add_argument('--datatool-name', '-n', required=True, help='Name of the datatool')
    parser.add_argument('--datatool-version', '-v', required=True, help="Version of datatool for uploading")
    parser.add_argument('--datatool-tag', '-t', required=True, help="Tag for the datatool for uploading")
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    storage_config = '../../config_files/storage_config.yml'
    storage_schema = '../../config_schemas/storage_schema.yml'
    upload(args.archive_path, args.datatool_name, args.datatool_version, args.datatool_tag, storage_config,
           storage_schema)


if __name__ == '__main__':
    main()
