from abc import ABC, abstractmethod
from typing import Union
from datetime import datetime
import sys
import os
import inspect
import copy
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, cur_path + '/../../')
from datatool_api.models.DTDataset import DTDataset
from custom_dataset_model import DTDatasetCustom


class DatatoolPatchInterface(ABC):
    @abstractmethod
    def create_patched_dataset(self, original_dataset: Union[DTDatasetCustom, DTDataset], operation_mode: str, **kwargs) -> Union[DTDatasetCustom, DTDataset]:
        pass

    def run_patch(self, original_dataset: Union[str, DTDatasetCustom, DTDataset], operation_mode: str,
                  validate_sample_paths: bool, original_dataset_dir, **kwargs):
        if type(original_dataset) == str:
            try:
                original_dataset = DTDatasetCustom(name='original_dataset',
                                                   operatingMode=operation_mode).load_from_json(
                    os.path.join(original_dataset_dir, 'dataset.json'))
            except Exception as e:
                original_dataset = DTDataset(name='original_dataset', operatingMode=operation_mode).load_from_json(
                    os.path.join(original_dataset_dir, 'dataset.json'))

        original_metadata = copy.deepcopy(original_dataset.metadata)

        try:
            patched_dataset = self.create_patched_dataset(original_dataset, operation_mode, **kwargs)
            if patched_dataset is not None:
                os.rename(os.path.join(original_dataset_dir, 'dataset.json'),
                          os.path.join(original_dataset_dir, 'original_dataset.json'))
                if os.path.exists(os.path.join(original_dataset_dir, 'dataset_sample.json')):
                    os.rename(os.path.join(original_dataset_dir, 'dataset_sample.json'),
                              os.path.join(original_dataset_dir, 'original_dataset_sample.json'))

                original_metadata.createdBy = 'datatool_patch.py'
                original_metadata.creationTime = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                patched_dataset.metadata = original_metadata
                patched_dataset.to_json(os.path.join(original_dataset_dir, 'dataset.json'),
                                        validate_sample_paths=validate_sample_paths)
                patched_dataset.to_json(os.path.join(original_dataset_dir, 'dataset_sample.json'),
                                        validate_sample_paths=validate_sample_paths, example_mode=True)
            else:
                raise Exception('create_patched_dataset method must return a dataset model instance')
        except Exception as e:
            print(e)
