import numpy as np
import math
import cv2


def vector_to_pitchyaw(vector: np.ndarray):
    """Using intrinsic rotation x (pitch) - y (yaw)

    :return: [pitch, yaw]
    :rtype: list[float]
    """
    vector = np.array(vector)
    vector = vector/ np.linalg.norm(vector)

    pitch = math.asin(vector[1])  # pitch (theta)
    yaw = math.atan2(vector[0], vector[2]) # yaw (phi)
    return [pitch, yaw]


def is_rotation_matrix(R: np.ndarray) :
    """Check if R is a valid rotation matrix

    :param R: Rotation Matrix
    :type R: numpy.ndarray
    :return: true if R is a rotation matrix
    :rtype: bool
    """
    Rt = np.transpose(R)
    should_be_identity = np.dot(Rt, R)
    I = np.identity(3, dtype=R.dtype)
    n = np.linalg.norm(I - should_be_identity)
    return n < 1e-6


def rotation_matrix_to_tayt_bryan_angles(R: np.ndarray) :
    """ Convention: Intrinsic rotation x-y'-z''

    :param R: Rotation matrix
    :type R: numpy.ndarray
    :return: Intrinsic x-y'-z'' tayt bryan angles
    :rtype: list[float]
    """
        
    assert(is_rotation_matrix(R))
    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])
    singular = sy < 1e-6
    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    return [x, y, z]


def rot_vec_to_tayt_bryan_angles(r: np.ndarray):
    """[Convention: Intrinsic rotation x-y'-z'' 

    :param r: Rotation vector
    :type r: numpy.ndarray
    :return: Intrinsic x-y'-z'' tayt bryan angles
    :rtype: list[float]
    """

    R, _ = cv2.Rodrigues(r)
    return rotation_matrix_to_tayt_bryan_angles(R)
