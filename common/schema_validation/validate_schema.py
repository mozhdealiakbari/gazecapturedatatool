import yaml
import jsonschema
import os
import json


def load_file(file_path):
    """
    Load a .json or .yml file
    :param file_path: Path to the .json or .yml file
    :return: dict with file contents
    """
    _, ext = os.path.splitext(file_path)
    with open(file_path) as f:
        if ext == '.yaml' or ext == '.yml':
            data = yaml.safe_load(f)
        elif ext == '.json':
            data = json.load(f)
        else:
            raise Exception('Wrong file format for loading')
    return data


def validate_schema(target_file, schema_file):
    """
    Validate a yml or json file content against a yml or json schema file

    :param target_file: The file to be validated
    :param schema_file: The schema used for validation
    :return: Parsed target_file if successfully validated, else Exception raised
    """
    parameters = load_file(target_file)
    parameters_schema = load_file(schema_file)
    jsonschema.validate(parameters, parameters_schema)
    return parameters
