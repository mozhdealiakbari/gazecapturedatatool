"""
This Module defines the basic models for Gaze capture dataset.

For more details on how to easily create your data models please refer to the documentation at:
            [https://gitlab.com/bonseyes/artifacts/data_tools/data-validation-api]
"""

from __future__ import annotations
from datatool_api.data_validation.base_model import BaseModel
from datatool_api.data_validation.attribute import Attribute
from typing import List, Dict, Union
from enum import Enum
from datatool_api.data_validation.containers.ValList import ValList
from datatool_api.data_validation.containers.StorageValDict import StorageValDict


class GazeCaptureBoundingBox2D(BaseModel):
    """
    Attributes:
        DotNum: Sequence number of the dot (starting from 0) being displayed during that frame.
		XPts, YPts: Position of the center of the dot (in points; see screen.json documentation below for more information on this unit) from the top-left corner of the screen.
		XCam, YCam: Position of the center of the dot in our prediction space. The position is measured in centimeters and is relative to the camera center, assuming the camera remains in a fixed position in space across all device orientations. 
		Time: Time (in seconds) since the displayed dot first appeared on the screen.
    """

    DotNum: int = Attribute(type=int)
    XPts: int = Attribute(type=int)
    YPts: int = Attribute(type=int)
    XCam: float = Attribute(type=float)
    YCam: float = Attribute(type=float)
    Time: float = Attribute(type=float)



    def __init__(self, DotNum: int = None,isValid: int = None ,XPts: int = None ,YPts: int = None ,XCam: float = None ,YCam: float = None,Time: float = None ):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureBoundingBox2D:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputDict: 
        :return: GazeCaptureBoundingBox2D
        """
        self.DotNum = int(inputDict['DotNum'])
        self.XPts = int(inputDict['XPts'])
        self.YPts = int(inputDict['YPts'])
        self.XCam = float(inputDict['XCam'])
        self.YCam = float(inputDict['YCam'])
        self.Time = float(inputDict['Time'])


        return self
class GazeCaptureBoundingBox(BaseModel):
    """
    Attributes:
        X, Y:float : Position of the top-left corner of the bounding box (in pixels). In appleFace.json, this value is relative to the top-left corner of the full frame; in appleLeftEye.json and appleRightEye.json, it is relative to the top-left corner of the face crop.
		W, H:float: Width and height of the bounding box (in pixels).
		IsValid: int Whether or not there was actually a detection. 1 = detection; 0 = no detection.
    """
    H: float = Attribute(type=float)
    W: float = Attribute(type=float)
    X: float = Attribute(type=float)
    Y: float = Attribute(type=float)
    isValid: int = Attribute(type=int)




    def __init__(self, H: float = None, W: float = None, X: float = None, Y: float = None, DotNum: int = None,
                 isValid: int = None  ):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureBoundingBox:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputList: Input array containing the bounding box values from Gaze Capture dataset
        :return: GazeCaptureBoundingBox
        """
        self.H = float(inputDict['H'])
        self.W = float(inputDict['W'])
        self.X = float(inputDict['X'])
        self.Y = float(inputDict['Y'])
        self.isValid = int(inputDict['IsValid'])



        return self
		
class GazeCaptureScreen(BaseModel):
    """
    Attributes:
        H, W:float: Height and width of the active screen area of the app (in points). This allows us to account for the iOS "Display Zoom" feature (which was used by some subjects) as well as larger status bars (e.g., when a Personal Hotspot is enabled) and split screen views (which was not used by any subjects). See this and this page for more information on the unit "points."
		Orientation:int: The orientation of the interface, as described by the enumeration UIInterface Orientation, where:
				1: portrait
				2: portrait, upside down (iPad only)
				3: landscape, with home button on the right
				4: landscape, with home button on the left
    """
    H: float = Attribute(type=float)
    W: float = Attribute(type=float)
    Orientation: int = Attribute(type=int)




    def __init__(self, H: float = None, W: float = None, Orientation: int = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureScreen:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputList: Input array containing the different elemnt of screen
        :return: GazeCaptureScreen
        """
        self.H = float(inputDict['H'])
        self.W = float(inputDict['W'])
        self.Orientation = int(inputDict['Orientation'])

        return self
		

class GazeCaptureXYZ(BaseModel):
    """
    Attributes:
        x: float: Top left x pixel value for the bounding box in the image
        y: float: Top left y pixel value for the bounding box in the image
        w: float: Width of the the bounding box in the image
        h: float: Height of the bounding box in the image
    """
    Z: float = Attribute(type=float)
    X: float = Attribute(type=float)
    Y: float = Attribute(type=float)
    

    def __init__(self, Z: float = None,  X: float = None, Y: float = None ):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureXYZ:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputList: Input array containing the bounding box values from Gaze Capture dataset
        :return: GazeCaptureXYZ
        """
        
        self.Y = float(inputDict['Y'])
        self.X = float(inputDict['X'])
        self.Z = float(inputDict['Z'])

        return self
		
class GazeCaptureXYZW(BaseModel):
    """
    Attributes:
        x: float: Top left x pixel value 
        y: float: Top left y pixel value 
        w: float: Width
    """
    Z: float = Attribute(type=float)
    X: float = Attribute(type=float)
    Y: float = Attribute(type=float)
    W: float = Attribute(type=float)


    def __init__(self, Z: float = None,  X: float = None, Y: float = None,W: float = None  ):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureXYZW:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputList: Input array containing the bounding box values from Gaze Capture dataset
        :return: GazeCaptureXYZW
        """
        

        self.X = float(inputDict['X'])
        self.W = float(inputDict['W'])
        self.Y = float(inputDict['Y'])
        self.Z = float(inputDict['Z'])

        return self

class GazeCaptureXY(BaseModel):
    """
    Attributes:
        x: float: Top left x pixel value 
        y: float: Top left y pixel value 
    X: float = Attribute(type=float)
    Y: float = Attribute(type=float)
  """  

    def __init__(self,  X: float = None, Y: float = None ):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeCaptureXY:
        """
        Extract the bounding box from the Gaze Capture bounding box array
        :param inputList: Input array containing the bounding box values from Gaze Capture dataset
        :return: GazeCaptureXY
        """
        self.X = float(inputDict['X'])
        self.Y = float(inputDict['Y'])

        return self


class GazeDatasetInfo(BaseModel):
    """
    Attributes:
        TotalFrames: int: Number of frames
        NumFaceDetections: int: Number of Face Detections in the image
        NumEyeDetections: int: Number of Eye Detections in the image
        Dataset: str: if Dataset is either train or test
        DeviceName: str: Device Name
    """
    TotalFrames: int = Attribute(type=int)
    NumFaceDetections: int = Attribute(type=int)
    NumEyeDetections: int = Attribute(type=int)
    Dataset: str = Attribute(type=str)
    DeviceName: str = Attribute(type=str)
    subject_ID: str = Attribute(type=str)

    def __init__(self, TotalFrames: int = 0,NumFaceDetections: int = 0, NumEyeDetections: int = 0, Dataset: str = None, DeviceName: str = None , subject_ID: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeDatasetInfo:
        """
        Extract dataset info from Gaze capture info.json dataset

        :param inputDict: Input dict containing the dataset info
        :return: GazeDatasetInfo
        """
        self.TotalFrames = int(inputDict['TotalFrames'])
        self.NumFaceDetections = int(inputDict['NumFaceDetections'])
        self.NumEyeDetections = int(inputDict['NumEyeDetections'])
        self.Dataset = str(inputDict['Dataset'])
        self.DeviceName = str(inputDict['DeviceName'])
        self.DeviceName = str(inputDict['subject_ID'])


        return self

class AttitudeRotationMatrixData(BaseModel):
    """
    Attributes:
        Matrix elements of Attitude Rotation
		all values are float
    """

    a: float = Attribute(type=float)
    b: float = Attribute(type=float)
    c: float = Attribute(type=float)
    d: float = Attribute(type=float)
    e: float = Attribute(type=float)
    f: float = Attribute(type=float)
    g: float = Attribute(type=float)
    h: float = Attribute(type=float)
    i: float = Attribute(type=float)

    def __init__(self, a: float = None, b: float = None, c: float = None , d: float = None, e: float = None, f: float = None, g: float = None, h: float = None, i: float = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputList: List[float]) -> AttitudeRotationMatrixData:
        """
        Extract a
        :param inputList: Input list of matrix elements
        :return:
        """
        
        self.a = float(inputList[0])
        self.b = float(inputList[1])
        self.c = float(inputList[2])
        self.d = float(inputList[3])        
        self.e = float(inputList[4])
        self.f = float(inputList[5])
        self.g = float(inputList[6])
        self.h = float(inputList[7])
        self.i = float(inputList[8])
        

        return self


class GazeMotionData(BaseModel):
    """
    Attributes:
        superCategory: str: Super category name for the category
        categoryId: str: Category Id
        categoryName: str: Category name
        keyPointLabels: ValList[CocoKeyPointLabel]: List of keypoint labels
        skeletonEdges: ValList[CocoSkeletonEdge]: List of skeleton edges
    """
    GravityX: GazeCaptureXYZ = Attribute(type=GazeCaptureXYZ)
    UserAcceleration: GazeCaptureXYZ = Attribute(type=GazeCaptureXYZ)
    AttitudeRotationMatrix: ValList[AttitudeRotationMatrixData] = Attribute(type=ValList)
    AttitudePitch: float = Attribute(type=float)
    Time: float = Attribute(type=float)
    AttitudeQuaternion: GazeCaptureXYZW = Attribute(type=GazeCaptureXYZW)
    AttitudeRoll: float = Attribute(type=float)
    RotationRate: GazeCaptureXYZ = Attribute(type=GazeCaptureXYZ)
    AttitudeYaw: float = Attribute(type=float)
    DotNum: int = Attribute(type=int)
    index: int = Attribute(type=int)
    subject_ID: str= Attribute(type=str)

    

	
    def __init__(self, GravityX: GazeCaptureXYZ = None, UserAcceleration: GazeCaptureXYZ = None, AttitudeRotationMatrix: ValList = None,AttitudePitch: float = None,Time: float = None,AttitudeQuaternion: GazeCaptureXYZW = None,AttitudeRoll: float = None, RotationRate: GazeCaptureXYZ = None,AttitudeYaw: float = None,DotNum: float = None, index: int = None,subject_ID: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeMotionData:
        """
        Extract  annotation category info from Gazecapture Motion.json file inside the directory

        :param inputDict: Input dict containing the annotation category info
        :return: GazeMotionData
        """
        self.GravityX = GazeCaptureXYZ().extract(inputDict['GravityX'])
        self.UserAcceleration = GazeCaptureXYZ().extract(inputDict['UserAcceleration'])
        self.AttitudeRotationMatrix.add(inputDict['AttitudeRotationMatrix'])
        self.AttitudePitch = float(inputDict['AttitudePitch'])
        self.Time = float(inputDict['Time'])
        self.AttitudeQuaternion = GazeCaptureXYZW().extract(inputDict['AttitudeQuaternion'])
        self.AttitudeRoll = float(inputDict['AttitudeRoll'])
        self.RotationRate = GazeCaptureXYZ().extract(inputDict['RotationRate'])
        self.AttitudeYaw = float(inputDict['AttitudeYaw'])
        self.DotNum = int(inputDict['DotNum'])
        self.subject_ID = str(inputDict['subject_ID'])
        self.index = int(inputDict['index'])


        return self


class GazeImageData(BaseModel):
    """
    Attributes:
        subject_ID: str: The subject Number
        categoryName: str: Category name
        appleFace: GazeCaptureBoundingBox : Dictionary of face bounding box and if face is valid or not
        skeletonEdges: ValList[CocoSkeletonEdge]: List of skeleton edges
    """
    subject_ID: str = Attribute(type=str)
    categoryName: str = Attribute(type=str)
    appleFace: GazeCaptureBoundingBox = Attribute(type=GazeCaptureBoundingBox)
    appleLeftEye: GazeCaptureBoundingBox = Attribute(type=GazeCaptureBoundingBox)
    appleRightEye: GazeCaptureBoundingBox = Attribute(type=GazeCaptureBoundingBox)
    dotInfo: GazeCaptureBoundingBox2D = Attribute(type=GazeCaptureBoundingBox2D)
    faceGrid: GazeCaptureBoundingBox = Attribute(type=GazeCaptureBoundingBox)
    screen: GazeCaptureScreen = Attribute(type=GazeCaptureScreen)
    image_ID : str = Attribute(type=str)
    frames : str = Attribute(type=str)

    def __init__(self, frames: str = None, subject_ID: str = None, appleFace: GazeCaptureBoundingBox = None, appleLeftEye: GazeCaptureBoundingBox = None,appleRightEye: GazeCaptureBoundingBox = None,dotInfo: GazeCaptureBoundingBox2D = None,faceGrid: GazeCaptureBoundingBox = None,screen: GazeCaptureScreen = None , image_ID: str = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def extract(self, inputDict: Dict) -> GazeImageData:
        """
        Extract  gaze image data from multiple files inside Gaze Capture for each image and subject.

        :param inputDict: Input dict containing the gaze information
        :return: GazeImageData
        """
        self.subject_ID = str(inputDict['subject_ID'])
        self.appleFace = GazeCaptureBoundingBox().extract(inputDict['appleFace'])
        self.appleLeftEye = GazeCaptureBoundingBox().extract(inputDict['appleLeftEye'])
        self.appleRightEye = GazeCaptureBoundingBox().extract(inputDict['appleRightEye'])
        self.dotInfo = GazeCaptureBoundingBox2D().extract(inputDict['dotInfo'])
        self.faceGrid = GazeCaptureBoundingBox().extract(inputDict['faceGrid'])
        self.screen = GazeCaptureScreen().extract(inputDict['screen'])
        self.image_ID = str(inputDict['image_ID'])
        self.frames = str(inputDict['frames'])

        return self