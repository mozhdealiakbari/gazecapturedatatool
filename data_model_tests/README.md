# Data Model Tests

This directory contains the unit test script which can be used to test if the data model classes correctly handle the annotations formatted as per the data model format. It contains a test_dataset.json file containing the annotations for a few samples as per the data model format, and it is used by the unit tests to detect any possible issues like wrong types, wrong data, wrong serialization and de-serialization etc.  

### How to Run
```
python3 data_model_tests.py
```
This will launch the unit tests and will generate a report file "DataModel_TestReport.out" containing the test results. 