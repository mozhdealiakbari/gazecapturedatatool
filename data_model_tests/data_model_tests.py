import unittest
import sys
import os
import inspect
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from datatool_api.models.DTDataset import DTDataset
from datatool_api.models.BaseTypes import DatasetMetadata
from custom_dataset_model import DTDatasetCustom
from custom_base_types import *


class TestDataModel(unittest.TestCase):

    test_json_path = 'test_dataset.json'
    dump_json_path = 'test_dataset_dump.json'

    # TODO: To be added by the developers
    expected_counts = {

    }

    def testJSONLoad(self):
        """
        Test if the test_dataset.json is loaded correctly using the data model classes

        :return:
        """
        # TODO: To be added by the developers
        pass

    def testJSONDump(self):
        """
        Test if loaded dataset can be dumped correctly to disk using the data model classes

        :return:
        """
        # TODO: To be added by the developers
        pass

    def testSampleCount(self):
        """
        Test if dumped dataset can be loaded back correctly from disk using the data model classes and the elements count are as expected

        :return:
        """
        # TODO: To be added by the developers
        pass


def cleanup(dump_json_path):
    if os.path.exists(dump_json_path):
        os.remove(dump_json_path)


if __name__ == '__main__':
    with open('DataModel_TestReport.out', "w") as f:
        runner = unittest.TextTestRunner(f, verbosity=4)
        unittest.main(testRunner=runner)
