from __future__ import annotations
from .attribute import Attribute
import typing
from typing import Dict, Any
from collections import defaultdict
from enum import Enum
from .validator import DTDataValidator
from .containers.ValDict import ValDict
from .containers.ValList import ValList
from .containers.StorageValDict import StorageValDict
from .containers.StorageValList import StorageValList


class BaseModel:
    __slots__ = ('is_stub', )
    _readMode: bool = False

    class _LoaderUtils:
        @staticmethod
        def handle_val_list(input_instance: BaseModel, attr_name: str, input_list: list, validator: Attribute,
                            doSet=True):
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            out = validator.constraints.get('type')(elem_validator, '_' + attr_name, input_instance)
            for v in input_list:
                out.add(BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                pass
            else:
                return out

        @staticmethod
        def handle_list(input_instance: BaseModel, attr_name: str, input_list: list, validator: Attribute, doSet=True):
            out = []
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for v in input_list:
                out.append(BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                setattr(input_instance, attr_name, out)
            else:
                return out

        @staticmethod
        def handle_val_dict(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute,
                            doSet=True):
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            out = validator.constraints.get('type')(elem_validator, '_' + attr_name, input_instance)
            for k, v in input_dict.items():
                out.add(k, BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False))
            if doSet is True:
                pass
            else:
                return out

        @staticmethod
        def handle_dict(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute, doSet=True):
            out = {}
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for k, v in input_dict.items():
                out[k] = BaseModel._get_mappers('load')[base_type](None, None, v, elem_validator, False)
            if doSet is True:
                setattr(input_instance, attr_name, out)
            else:
                return out

        @staticmethod
        def handle_base_model(input_instance: BaseModel, attr_name: str, input_dict: dict, validator: Attribute,
                              doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, validator.constraints.get('type')().from_dict(input_dict))
            else:
                return validator.constraints.get('type')().from_dict(input_dict)

        @staticmethod
        def handle_enum(input_instance: BaseModel, attr_name: str, input_value: str, validator: Attribute, doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, validator.constraints.get('type')(input_value))
            else:
                return validator.constraints.get('type')(input_value)

        @staticmethod
        def handle_others(input_instance: BaseModel, attr_name: str, input_value: str, validator: Attribute,
                          doSet=True):
            if doSet is True:
                setattr(input_instance, attr_name, input_value)
            else:
                return input_value

    class _DumperUtils:
        @staticmethod
        def handle_list(input_instance: BaseModel, attr_name: str, attr_value, validator: Attribute,
                        output: dict, doSet=True):
            out = []
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for v in attr_value:
                out.append(BaseModel._get_mappers('dump')[base_type](input_instance, 'list element', v, elem_validator,
                                                                     out, False))
            if doSet is True:
                output[attr_name] = out
            else:
                return out

        @staticmethod
        def handle_dict(input_instance: BaseModel, attr_name: str, attr_value, validator: Attribute,
                        output: dict, doSet=True):
            out = {}
            elem_validator = validator.constraints.get('element_constraints', Attribute())
            base_type = getattr(elem_validator, '_baseType', object)
            for k, v in attr_value.items():
                out[k] = BaseModel._get_mappers('dump')[base_type](input_instance, k, v, elem_validator, out, False)
            if doSet is True:
                output[attr_name] = out
            else:
                return out

        @staticmethod
        def handle_base_model(input_instance: BaseModel, attr_name: str, attr_value, validator: Attribute,
                              output: dict, doSet=True):
            if doSet is True:
                output[attr_name] = attr_value.dict()
            else:
                return attr_value.dict()

        @staticmethod
        def handle_enum(input_instance: BaseModel, attr_name: str, attr_value, validator: Attribute,
                        output: dict, doSet=True):
            if doSet is True:
                output[attr_name] = attr_value.value
            else:
                return attr_value.value

        @staticmethod
        def handle_others(input_instance: BaseModel, attr_name: str, attr_value, validator: Attribute,
                          output: dict, doSet=True):
            if doSet is True:
                output[attr_name] = attr_value
            else:
                return attr_value

    @staticmethod
    def _create_property_funcs(name, attribute: Attribute):
        def getter(ins):
            _out = getattr(ins, '_' + name, None)
            if _out is None:
                if issubclass(attribute.constraints.get('type', object),
                              (ValDict, ValList, StorageValDict, StorageValList)):
                    elem_constraints = attribute.constraints.get('element_constraints', Attribute())
                    return attribute.constraints['type'](elem_constraints, '_' + name, ins)
                elif BaseModel._readMode is True and issubclass(attribute.constraints.get('type', object), BaseModel):
                    _out = attribute.constraints['type']()
                    _out.is_stub = True
                    return _out
                else:
                    return _out
            else:
                return _out

        def setter(ins, value):
            attribute.validate(name, value)
            setattr(ins, '_' + name, value)

        return getter, setter

    @staticmethod
    def _create_validators(input_class, attribute_name, attribute_val, validators_dict, default_attrs_dict):
        setattr(input_class, attribute_name, None)
        if attribute_name[0] != '_':
            setattr(input_class, attribute_name,
                    property(*BaseModel._create_property_funcs(attribute_name, attribute_val)))
        validators_dict[attribute_name.strip('_')] = attribute_val
        if attribute_val.constraints.get('default', None) is not None:
            default_attrs_dict.append(attribute_name.strip('_'))

        # Add Typing info for loaders
        _cur_attribute = attribute_val
        while _cur_attribute is not None:
            _attribute_type = _cur_attribute.constraints.get('type', object)
            if issubclass(_attribute_type, Enum):
                setattr(_cur_attribute, '_baseType', Enum)
            elif issubclass(_attribute_type, BaseModel):
                setattr(_cur_attribute, '_baseType', BaseModel)
            elif issubclass(_attribute_type, (list, dict, ValList, StorageValList, ValDict, StorageValDict)):
                setattr(_cur_attribute, '_baseType', _attribute_type)
            else:
                setattr(_cur_attribute, '_baseType', object)
            _cur_attribute = _cur_attribute.constraints.get('element_constraints', None)

    def __new__(cls, *args, **kwargs):
        _instance = object.__new__(cls)
        if not hasattr(cls, '__validators__'):
            _validators = {}
            _default_attributes = []
            _current = cls
            while _current != BaseModel and _current != object:
                for _annotation_name, _annotation_type in _current.__dict__.get('__annotations__', {}).items():
                    _attribute = _current.__dict__.get(_annotation_name, Attribute(type=_annotation_type))
                    BaseModel._create_validators(_current, _annotation_name, _attribute, _validators,
                                                 _default_attributes)
                for _annotation_name, _attribute in _current.__dict__.items():
                    if type(_attribute) == Attribute:
                        BaseModel._create_validators(_current, _annotation_name, _attribute, _validators,
                                                     _default_attributes)
                _current = _current.__base__
            setattr(cls, '__validators__', _validators)
            setattr(cls, '__default_attributes__', _default_attributes)

        for _annotation_name in cls.__default_attributes__:
            setattr(_instance, _annotation_name, cls.__validators__[_annotation_name].constraints.get('default'))

        _instance.is_stub = False
        return _instance

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if v is not None and k in self.__class__.__validators__:
                setattr(self, k, v)

    @staticmethod
    def _traverse(input_class, base, structure, rev_structure):
        _current = input_class
        while _current != BaseModel and _current != object:
            for key, value in _current.__dict__['__annotations__'].items():
                if type(value) == typing.GenericAlias or type(value) == typing._GenericAlias:
                    structure[value].append(base + '.' + key)
                    rev_structure[base + '.' + key] = value
                elif issubclass(value, BaseModel):
                    structure[value].append(base + '.' + key)
                    rev_structure[base + '.' + key] = value
                    value._traverse(value, base + '.' + key, structure, rev_structure)
            _current = _current.__base__

    @staticmethod
    def _get_mappers(typeStr: str):
        return {
            'load': {
                list: BaseModel._LoaderUtils.handle_list,
                ValList: BaseModel._LoaderUtils.handle_val_list,
                StorageValList: BaseModel._LoaderUtils.handle_val_list,
                dict: BaseModel._LoaderUtils.handle_dict,
                ValDict: BaseModel._LoaderUtils.handle_val_dict,
                StorageValDict: BaseModel._LoaderUtils.handle_val_dict,
                BaseModel: BaseModel._LoaderUtils.handle_base_model,
                Enum: BaseModel._LoaderUtils.handle_enum,
                object: BaseModel._LoaderUtils.handle_others
            },
            'dump': {
                list: BaseModel._DumperUtils.handle_list,
                ValList: BaseModel._DumperUtils.handle_list,
                StorageValList: BaseModel._DumperUtils.handle_list,
                dict: BaseModel._DumperUtils.handle_dict,
                ValDict: BaseModel._DumperUtils.handle_dict,
                StorageValDict: BaseModel._DumperUtils.handle_dict,
                BaseModel: BaseModel._DumperUtils.handle_base_model,
                Enum: BaseModel._DumperUtils.handle_enum,
                object: BaseModel._DumperUtils.handle_others
            }
        }[typeStr]

    def _generate_structure(self):
        if not hasattr(self.__class__, '_structure'):
            self.__class__._structure = defaultdict(list)
            self.__class__._rev_structure = dict()
            self._traverse(self.__class__, '', self.__class__._structure, self.__class__._rev_structure)

    @staticmethod
    def _create_flatten(output, parent_name, name, value):
        if isinstance(value, list):
            parent_name = parent_name + '.' + name if parent_name else name
            for i in range(len(value)):
                name = '_' + str(i)
                BaseModel._create_flatten(output, parent_name, name, value[i])
        elif isinstance(value, dict):
            parent_name = parent_name + '.' + name if parent_name else name
            for key, val in value.items():
                name = str(key)
                BaseModel._create_flatten(output, parent_name, name, val)
        else:
            new_name = parent_name + '.' + name if parent_name else name
            output[new_name] = value

    def set_attribute(self, name, value: Any) -> None:
        """
        Set the value for an instance attribute

        :param name: Name of the attribute
        :param value: Value for the attribute
        :return: None
        """
        if name in self.__class__.__validators__:
            setattr(self, name, value)
        else:
            raise NameError('No attribute with name: {} found'.format(name))

    def get_attribute(self, name: str) -> Any:
        """
        Get the value for an instance attribute

        :param name: Name of the attribute
        :return: Any
        """
        return getattr(self, name)

    def validate(self) -> None:
        """
        Explicitly validate all attributes of an instance, irrespective of the validation setting

        :return:
        """
        current_validation_setting = DTDataValidator.applyValidation
        if DTDataValidator.applyValidation is False:
            DTDataValidator.applyValidation = True
        if hasattr(self, '__dict__'):
            for key, val in self.__dict__.items():
                key = key.strip('_')
                if key in self.__class__.__validators__:
                    self.__class__.__validators__[key].validate(key, val)
        elif hasattr(self, '__slots__'):
            for key in self.__slots__:
                val = getattr(self, key)
                key = key.strip('_')
                if key in self.__class__.__validators__:
                    self.__class__.__validators__[key].validate(key, val)

        DTDataValidator.applyValidation = current_validation_setting

    def dict(self) -> Dict:
        """
        Create dictionary for the object

        :return: dict
        """
        output = {}
        if hasattr(self, '__dict__'):
            for key, val in self.__dict__.items():
                key = key.strip('_')
                if key in self.__class__.__validators__:
                    base_type = getattr(self.__class__.__validators__[key], '_baseType', object)
                    BaseModel._get_mappers('dump')[base_type](self, key, val, self.__class__.__validators__[key], output,
                                                             True)
        elif hasattr(self, '__slots__'):
            for key in self.__slots__:
                val = getattr(self, key)
                key = key.strip('_')
                if key in self.__class__.__validators__:
                    base_type = getattr(self.__class__.__validators__[key], '_baseType', object)
                    BaseModel._get_mappers('dump')[base_type](self, key, val, self.__class__.__validators__[key], output,
                                                             True)
        return output

    def from_dict(self, input_dict: dict) -> BaseModel:
        """
        Loads an object from dictionary

        :param input_dict: Input dictionary
        :return: BaseModel
        """
        for key, value in input_dict.items():
            if value is not None:
                base_type = getattr(self.__class__.__validators__[key], '_baseType', object)
                BaseModel._get_mappers('load')[base_type](self, key, value, self.__class__.__validators__[key], True)
        return self

    def flatten(self, output: Dict, start_name='') -> None:
        """
        Fully flatten an object to the lowest level

        :param output: Dictionary to stored the flattened object
        :param start_name: If a basename is specified for the object
        :return: None
        """
        inter_dict = self.dict()
        for k, v in inter_dict.items():
            self._create_flatten(output, parent_name=start_name, name=k, value=v)
