from ..attribute import Attribute
from typing import MutableMapping, Generic, Iterator, ItemsView, KeysView, ValuesView, Optional, Any
from typing import Mapping, Tuple
from typing import TypeVar


_T = TypeVar("_T")
_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_VT_co = TypeVar("_VT_co", covariant=True)
_T_co = TypeVar("_T_co", covariant=True)


class ValDict(MutableMapping[_KT, _VT], Generic[_KT, _VT]):
    def __init__(self, validator: Attribute = None, name: str = None, parent: object = None):
        self._name = name
        self._parent = parent
        self._validator = validator
        self._data = {}

    def __setitem__(self, k: _KT, v: _VT) -> None:
        if self._validator is not None:
            self._validator.validate(str(k), v)
        self._data[k] = v
        if (self._parent and self._name) is not None and getattr(self._parent, self._name, None) is None:
            setattr(self._parent, self._name, self)

    def __getitem__(self, k: _KT) -> _VT_co:
        return self._data[k]

    def get(self, key: _KT, default: _VT_co = None) -> Optional[_VT_co]:
        return self._data.get(key, default)

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[_T_co]:
        return iter(self._data)

    def __contains__(self, key: _KT) -> bool:
        return key in self._data

    def items(self) -> ItemsView[_KT, _VT]:
        return self._data.items()

    def keys(self) -> KeysView[_KT]:
        return self._data.keys()

    def values(self) -> ValuesView[_VT]:
        return self._data.values()

    def pop(self, key: _KT, default: _VT = None) -> _VT:
        return self._data.pop(key, default)

    def has_key(self, key: _KT) -> bool:
        return key in self._data

    def add(self, k: _KT, v: _VT) -> None:
        self.__setitem__(k, v)

    def item_count(self) -> int:
        return len(self._data)

    def clear(self) -> None:
        self._data.clear()

    def popitem(self) -> Tuple[_KT, _VT]:
        return self._data.popitem()

    def __delitem__(self, v: _KT) -> None:
        self._data.__delitem__(v)

    def setdefault(self, key: _KT, default: _VT = ...) -> _VT:
        raise NotImplementedError

    def update(self, __m: Mapping[_KT, _VT], **kwargs: _VT) -> None:
        raise NotImplementedError
