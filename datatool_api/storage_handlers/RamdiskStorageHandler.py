import json
from ..data_validation.base_model import BaseModel
from ..interfaces.StorageIOInterface import StoragePointer, StorageHandlerInterface


class RamdiskStoragePointer(StoragePointer):
	__slots__ = ["content"]
	
	def __init__(self, object_id, class_type, content):
		super().__init__(object_id, class_type)
		self.content = content


class RamdiskStorageHandler(StorageHandlerInterface):	
	def write(self, object_id, data_category: str, data_object: BaseModel) -> RamdiskStoragePointer:
		content = json.dumps(data_object.dict())
		sample = RamdiskStoragePointer(object_id, type(data_object), content)
		return sample
	
	def read(self, pointer: RamdiskStoragePointer, remove=False) -> BaseModel:
		data = json.loads(pointer.content)
		out_object = pointer.type().from_dict(data)
		return out_object
	
	def close(self):
		pass

	def remove(self, pointer: RamdiskStoragePointer) -> None:
		pointer.content = None
