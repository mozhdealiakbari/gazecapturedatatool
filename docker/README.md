# Datatool Docker Usage  

## To build the docker image  
Navigate to docker subdirectory and execute `build_image.sh`
```
cd docker
source build_image.sh
```

## To Push the docker image to gitlab container registry 
Navigate to docker subdirectory and execute `push_image.sh`
```
cd docker
source push_image.sh
```

## To run the data tool once you have built the image  
Navigate to the docker subdirectory
```
cd docker
```

### If dataset is not pre-downloaded
Download the relevant dataset version and place the unzipped dataset contents on the local file system. For download instructions refer to [How to download the source dataset](../README.md#how-to-download-the-source-dataset).

### If dataset is pre-downloaded and stored in a remote storage (s3 or synology NAS):  
Set user credentials in the environment variables
  - If using synology for remote storage:
  ```
  export DATATOOL_USER="<Username>"
  export DATATOOL_PASSWORD="<Password>"
  ```
  - If using amazon s3 for remote storage:
  ```  
  export AWS_ACCESS_KEY="<aws_access_key>"
  export AWS_ACCESS_KEY="<aws_secret_key>"
  ```

### Run datatool
To process the dataset and generate the standardized output, execute `run.sh` script
``` 
  source run.sh

  Required arguments:
    --output-dir, -o            Directory where to dump the output of datatool


  Optional version arguments:
    --image-tag, -i             Tag of the image to run, if not specified, the image tag which was used to build the image is used
    --container-name, -c        Name to be given to the docker container, if not specified, default value is used
    --version, -v               Version of the datatool to run, this overrides the current version set in the datatool config
    --tags, -t                  List of tags to be processed (if not specified, all available tags for the datatool version are processed)


  Optional operational arguments:
  --operation-mode, -om         Operation mode for the datatool, this overrides the default mode set in datatool config
  --validate-sample-paths, -vl  If sample paths must be validated while dumping the dataset json to disk, this overrides the default value set in datatool config


  Advance optional arguments:
    --storage, -s               Type of storage, [synology, s3 or local], this value overrides the default storage set in datatool config
    --storage-loc, -l           Address for remote storage (only used if storage is of type synology), this value overrides the location set in datatool config
    --storage-root, -r          Root directory holding the dataset:
                                For local storage: absolute path to the dataset root directory,
                                For synology storage: path to the synology root directory,
                                For s3 storage: bucket name of the bucket holding the dataset
    --no-mount, -n              Boolean flag, add it if you DO NOT WANT to MOUNT the output directory to docker and let docker write the output directly to it,
                                if this flag is specified, docker first creates the output internally and then output gets copied from docker to host
                                [This takes more space and time but more secure as docker never writes to the host directly]
```

## To export the docker image as tar file  
```
docker save <Image> > <Image>.tar
```

## To load the docker image from tar file  
```
docker load < <Image>.tar
```
