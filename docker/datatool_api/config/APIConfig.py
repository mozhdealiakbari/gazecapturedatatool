from ..data_validation.validator import DTDataValidator
from ..data_validation.base_model import BaseModel


class DTAPIConfig:

    @staticmethod
    def enable_validation() -> None:
        """
        Enable the data validation for all model instances

        :return: None
        """
        DTDataValidator.applyValidation = True

    @staticmethod
    def disable_validation() -> None:
        """
        Disable the data validation for all model instances

        :return: None
        """
        DTDataValidator.applyValidation = False

    @staticmethod
    def is_validation_enabled() -> bool:
        """
        If the validation mode is enabled at present

        :return: bool
        """
        return DTDataValidator.applyValidation


    @staticmethod
    def enable_read_mode() -> None:
        """
        Enables the read mode for all model instances. Read mode is helpful when the user is interested in reading the
        leaf objects. By enabling the read mode user does not need to check for the existence of intermediate container
        objects while accessing a leaf.

        Example:

            With read mode disabled:
        if (subject.actionUnits is not None and subject.actionUnits.AU_10 is not None
        and subject.actionUnits.AU_10.activated is not None): out = subject.actionUnits.AU_10.activated

            With read mode enabled:
        if subject.actionUnits.AU_10.activated is not None: out = subject.actionUnits.AU_10.activated

            In the second case, read mode generates the the hollow objects on the fly to user can assume all intermediate
        objects exists and do not need to check for their existence explicitly.

        Note: Trying to add or change the data for an object for which read mode is on, can have unexpected results.
              Please DO NOT add or change data for an object when read mode is on, to avoid unexpected results.

        :return: None
        """
        BaseModel._readMode = True

    @staticmethod
    def disable_read_mode() -> None:
        """
        Disable the read mode for the Data Tool

        :return: None
        """
        BaseModel._readMode = False

    @staticmethod
    def is_read_mode_enabled() -> bool:
        """
        If the read mode is enabled at present

        :return: bool
        """
        return BaseModel._readMode
