from ..data_validation.base_model import BaseModel
from .StorageHandlerInterface import StorageHandlerInterface, StoragePointer


class StorageIOInterface:
	def __init__(self, storage_handler: StorageHandlerInterface, operatingMode: str):
		self._storage_handler = storage_handler
		self.operatingMode = operatingMode

	def write_to_storage(self, object_id, data_category: str, data_object: BaseModel) -> StoragePointer:
		return self._storage_handler.write(object_id, data_category, data_object)
	
	def read_from_storage(self, pointer: StoragePointer, remove: bool = False) -> BaseModel:
		return self._storage_handler.read(pointer, remove)

	def remove_from_storage(self, pointer: StoragePointer):
		self._storage_handler.remove(pointer)

	def close(self):
		self._storage_handler.close()
