from enum import Enum


class Categories:
    class Object(Enum):
        childSeat = 'childSeat'
        childInSeat = 'childInSeat'
        dog = 'dog'
        cat = 'cat'
        cellphone = 'cellphone'
        handbag = 'handbag'
        backpack = 'backpack'
        laptop = 'laptop'
        bottle = 'bottle'
        book = 'book'
        teddyBear = 'teddyBear'
        doll = 'doll'
        umbrella = 'umbrella'
        box = 'box'
        petCarryCage = 'petCarryCage'
        wallet = 'wallet'

    class Emotion(Enum):
        neutral = 'neutral'
        anger = 'anger'
        disgust = 'disgust'
        fear = 'fear'
        happiness = 'happiness'
        sadness = 'sadness'
        surprise = 'surprise'
        contempt = 'contempt'

    class ActionUnit(Enum):
        AU_10 = 'AU_10'
        AU_11 = 'AU_11'
        AU_12 = 'AU_12'
        AU_13 = 'AU_13'
        AU_14 = 'AU_14'
        AU_15 = 'AU_15'
        AU_16 = 'AU_16'
        AU_17 = 'AU_17'
        AU_18 = 'AU_18'
        AU_19 = 'AU_19'
        AU_20 = 'AU_20'
        AU_21 = 'AU_21'
        AU_22 = 'AU_22'
        AU_23 = 'AU_23'
        AU_24 = 'AU_24'
        AU_25 = 'AU_25'
        AU_26 = 'AU_26'
        AU_27 = 'AU_27'
        AU_28 = 'AU_28'
        AU_29 = 'AU_29'
        AU_30 = 'AU_30'
        AU_31 = 'AU_31'
        AU_32 = 'AU_32'
        AU_33 = 'AU_33'
        AU_34 = 'AU_34'
        AU_35 = 'AU_35'
        AU_36 = 'AU_36'
        AU_37 = 'AU_37'
        AU_38 = 'AU_38'
        AU_39 = 'AU_39'
        AU_40 = 'AU_40'
        AU_41 = 'AU_41'
        AU_42 = 'AU_42'
        AU_43 = 'AU_43'
        AU_44 = 'AU_44'
        AU_45 = 'AU_45'
        AU_46 = 'AU_46'
        AU_01 = 'AU_01'
        AU_02 = 'AU_02'
        AU_04 = 'AU_04'
        AU_05 = 'AU_05'
        AU_06 = 'AU_06'
        AU_07 = 'AU_07'
        AU_08 = 'AU_08'
        AU_09 = 'AU_09'
        AU_51 = 'AU_51'
        AU_52 = 'AU_52'
        AU_53 = 'AU_53'
        AU_54 = 'AU_54'
        AU_55 = 'AU_55'
        AU_56 = 'AU_56'
        AU_57 = 'AU_57'
        AU_58 = 'AU_58'
        AU_M55 = 'AU_M55'
        AU_M56 = 'AU_M56'
        AU_M57 = 'AU_M57'
        AU_M59 = 'AU_M59'
        AU_M60 = 'AU_M60'
        AU_M83 = 'AU_M83'
        AU_61 = 'AU_61'
        AU_62 = 'AU_62'
        AU_63 = 'AU_63'
        AU_64 = 'AU_64'
        AU_65 = 'AU_65'
        AU_66 = 'AU_66'
        AU_69 = 'AU_69'
        AU_M61 = 'AU_M61'
        AU_M62 = 'AU_M62'
        AU_M68 = 'AU_M68'
        AU_M69 = 'AU_M69'
        AU_70 = 'AU_70'
        AU_71 = 'AU_71'
        AU_72 = 'AU_72'
        AU_73 = 'AU_73'
        AU_74 = 'AU_74'
        AU_50 = 'AU_50'
        AU_80 = 'AU_80'
        AU_81 = 'AU_81'
        AU_82 = 'AU_82'
        AU_84 = 'AU_84'
        AU_85 = 'AU_85'
        AU_91 = 'AU_91'
        AU_92 = 'AU_92'
        AU_97 = 'AU_97'
        AU_98 = 'AU_98'

    class ActionUnitPhase(Enum):
        onset = 'onset'
        apex = 'apex'
        offset = 'offset'

    class ActionUnitIntensity(Enum):
        A = 'A'
        B = 'B'
        C = 'C'
        D = 'D'
        E = 'E'

    class ActionUnitSymmetry(Enum):
        left = 'left'
        right = 'right'
        both = 'both'

    class Gender(Enum):
        male = 'male'
        female = 'female'
        other = 'other'

    class AgeGroup(Enum):
        AG_0_2 = 'AG_0_2'
        AG_3_9 = 'AG_3_9'
        AG_10_19 = 'AG_10_19'
        AG_20_29 = 'AG_20_29'
        AG_30_39 = 'AG_30_39'
        AG_40_49 = 'AG_40_49'
        AG_50_59 = 'AG_50_59'
        AG_60_69 = 'AG_60_69'
        AG_70_more = 'AG_70_more'

    class Ethnicity(Enum):
        white = 'white'
        black = 'black'
        asian = 'asian'
        latino = 'latino'
        indian = 'indian'
        arabic = 'arabic'
        middleEastern = 'middleEastern'
        nativeAmerican = 'nativeAmerican'
        pacificIslander = 'pacificIslander'
        other = 'other'

    class EyesClosed(Enum):
        closed = 'closed'
        opened = 'opened'
        halfOpened = 'halfOpened'

    class FacialHair(Enum):
        noBeard = 'noBeard'
        fullBeard = 'fullBeard'
        mustache = 'mustache'

    class Glasses(Enum):
        noGlasses = 'noGlasses'
        correctionGlasses = 'correctionGlasses'
        sunGlasses = 'sunGlasses'

    class HairStyle(Enum):
        bald = 'bald'
        long = 'long'
        short = 'short'

    class HairColor(Enum):
        blond = 'blond'
        brunette = 'brunette'
        red = 'red'
        black = 'black'
        white = 'white'
        other = 'other'

    class SkinType(Enum):
        fair = 'fair'
        medium = 'medium'
        black = 'black'

    class Clothes(Enum):
        tShirt = 'tShirt'
        pulloverJacket = 'pulloverJacket'
        thickWinterJacket = 'thickWinterJacket'
        hat = 'hat'
        baseballCap = 'baseballCap'
        hoodie = 'hoodie'
        mask = 'mask'

    class BodyType(Enum):
        fullBody = 'fullBody'
        upperBody = 'upperBody'
        lowerBody = 'lowerBody'

    class EyeLabel(Enum):
        leftEye = 'leftEye'
        rightEye = 'rightEye'

    class HandLabel(Enum):
        leftHand = 'leftHand'
        rightHand = 'rightHand'

    class FootLabel(Enum):
        leftFoot = 'leftFoot'
        rightFoot = 'rightFoot'

    class FaceOrientation(Enum):
        fullLeftProfile = 'fullLeftProfile'
        halfLeftProfile = 'halfLeftProfile'
        straight = 'straight'
        halfRightProfile = 'halfRightProfile'
        fullRightProfile = 'fullRightProfile'

    class EyesOrientation(Enum):
        left = 'left'
        straight = 'straight'
        right = 'right'

    class PainAFF(Enum):
        A = 'A'
        B = 'B'
        C = 'C'
        D = 'D'
        E = 'E'
        F = 'F'
        G = 'G'
        H = 'H'
        I = 'I'
        J = 'J'
        K = 'K'
        L = 'L'
        M = 'M'
        N = 'N'
        O = 'O'
        P = 'P'
        Q = 'Q'

    class PainSEN(Enum):
        A = 'A'
        B = 'B'
        C = 'C'
        D = 'D'
        E = 'E'
        F = 'F'
        G = 'G'
        H = 'H'
        I = 'I'
        J = 'J'
        K = 'K'
        L = 'L'
        M = 'M'
        N = 'N'
        O = 'O'
        P = 'P'
        Q = 'Q'

    class PainOPR(Enum):
        A = 'A'
        B = 'B'
        C = 'C'
        D = 'D'
        E = 'E'
        F = 'F'

    class Weather(Enum):
        rainy = 'rainy'
        sunny = 'sunny'
        cloudy = 'cloudy'
        clear = 'clear'
        snowy = 'snowy'

    class DayTime(Enum):
        morning = 'morning'
        afternoon = 'afternoon'
        night = 'night'

    class OutdoorLight(Enum):
        daytime = 'daytime'
        transitionOfLightning = 'transitionOfLightning'
        nightTime = 'nightTime'
        fullyDark = 'fullyDark'
        sunlightFalling = 'sunlightFalling'

    class IndoorLight(Enum):
        pass

    class ColorSpace(Enum):
        grayscale = 'grayscale'
        rgb = 'rgb'
        infrared = 'infrared'
        yuv = 'yuv'
        hsi = 'hsi'

    class LandmarkType(Enum):
        L2d = 'L2d'
        L3d = 'L3d'

    class SkeletonType(Enum):
        S2d = 'S2d'
        S3d = 'S3d'

    class LandmarkRegion(Enum):
        face = 'face'
        body = 'body'
        leftEye = 'leftEye'
        rightEye = 'rightEye'
        leftHand = 'leftHand'
        rightHand = 'rightHand'
        leftFoot = 'leftFoot'
        rightFoot = 'rightFoot'
        object = 'object'

    class BoundingBoxRegion(Enum):
        face = 'face'
        body = 'body'
        leftEye = 'leftEye'
        rightEye = 'rightEye'
        leftHand = 'leftHand'
        rightHand = 'rightHand'
        leftFoot = 'leftFoot'
        rightFoot = 'rightFoot'
        object = 'object'

    class SkeletonRegion(Enum):
        face = 'face'
        body = 'body'
        leftEye = 'leftEye'
        rightEye = 'rightEye'
        leftHand = 'leftHand'
        rightHand = 'rightHand'
        leftFoot = 'leftFoot'
        rightFoot = 'rightFoot'
        object = 'object'

    class SegmentationRegion(Enum):
        face = 'face'
        body = 'body'
        leftEye = 'leftEye'
        rightEye = 'rightEye'
        leftHand = 'leftHand'
        rightHand = 'rightHand'
        leftFoot = 'leftFoot'
        rightFoot = 'rightFoot'
        object = 'object'

    class SegmentationType(Enum):
        polygon = 'polygon'
        RLE = 'RLE'

    class HandsOnWheel(Enum):
        both = 'both'
        left = 'left'
        right = 'right'
        none = 'none'

    class AudioKeyword(Enum):
        pass

    metadata = {
        "samples.objects": {
            "type": "single_label",
            "categories": {
                "childSeat": {
                    "id": 1,
                    "name": "childSeat",
                    "color": "#F57453"
                },
                "childInSeat": {
                    "id": 2,
                    "name": "childInSeat",
                    "color": "#AC6AFC"
                },
                "dog": {
                    "id": 3,
                    "name": "dog",
                    "color": "#6CE3E6"
                },
                "cat": {
                    "id": 4,
                    "name": "cat",
                    "color": "#C5FC6A"
                },
                "cellphone": {
                    "id": 5,
                    "name": "cellphone",
                    "color": "#F5A540"
                },
                "handbag": {
                    "id": 6,
                    "name": "handbag",
                    "color": "#F58B4C"
                },
                "backpack": {
                    "id": 7,
                    "name": "backpack",
                    "color": "#DA62FC"
                },
                "laptop": {
                    "id": 8,
                    "name": "laptop",
                    "color": "#65BAE6"
                },
                "bottle": {
                    "id": 9,
                    "name": "bottle",
                    "color": "#82FC62"
                },
                "book": {
                    "id": 10,
                    "name": "book",
                    "color": "#F5BA38"
                },
                "teddyBear": {
                    "id": 11,
                    "name": "teddyBear",
                    "color": "#FC74B7"
                },
                "doll": {
                    "id": 12,
                    "name": "doll",
                    "color": "#768BE6"
                },
                "umbrella": {
                    "id": 13,
                    "name": "umbrella",
                    "color": "#74FCA4"
                },
                "box": {
                    "id": 14,
                    "name": "box",
                    "color": "#F5DF49"
                },
                "petCarryCage": {
                    "id": 15,
                    "name": "petCarryCage",
                    "color": "#FC3242"
                },
                "wallet": {
                    "id": 16,
                    "name": "wallet",
                    "color": "#403AE6"
                }
            }
        },
        "samples.subjects.emotions": {
            "type": "multi_label",
            "categories": {
                "neutral": {
                    "id": 1,
                    "name": "neutral",
                    "color": "#807A7C"
                },
                "anger": {
                    "id": 2,
                    "name": "anger",
                    "color": "#424142"
                },
                "disgust": {
                    "id": 3,
                    "name": "disgust",
                    "color": "#7330A4"
                },
                "fear": {
                    "id": 4,
                    "name": "fear",
                    "color": "#94D352"
                },
                "happiness": {
                    "id": 5,
                    "name": "happiness",
                    "color": "#FF0084"
                },
                "sadness": {
                    "id": 6,
                    "name": "sadness",
                    "color": "#00B2F7"
                },
                "surprise": {
                    "id": 7,
                    "name": "surprise",
                    "color": "#FFCB29"
                },
                "contempt": {
                    "id": 8,
                    "name": "contempt",
                    "color": "#A8324E"
                }
            }
        },
        "samples.subjects.actionUnits.main": {
            "type": "multi_label",
            "categories": {
                "AU_10": {
                    "id": 9,
                    "name": "AU_10",
                    "color": "#578bd4"
                },
                "AU_11": {
                    "id": 10,
                    "name": "AU_11",
                    "color": "#578bd4"
                },
                "AU_12": {
                    "id": 11,
                    "name": "AU_12",
                    "color": "#578bd4"
                },
                "AU_13": {
                    "id": 12,
                    "name": "AU_13",
                    "color": "#578bd4"
                },
                "AU_14": {
                    "id": 13,
                    "name": "AU_14",
                    "color": "#578bd4"
                },
                "AU_15": {
                    "id": 14,
                    "name": "AU_15",
                    "color": "#578bd4"
                },
                "AU_16": {
                    "id": 15,
                    "name": "AU_16",
                    "color": "#578bd4"
                },
                "AU_17": {
                    "id": 16,
                    "name": "AU_17",
                    "color": "#578bd4"
                },
                "AU_18": {
                    "id": 17,
                    "name": "AU_18",
                    "color": "#578bd4"
                },
                "AU_19": {
                    "id": 18,
                    "name": "AU_19",
                    "color": "#578bd4"
                },
                "AU_20": {
                    "id": 19,
                    "name": "AU_20",
                    "color": "#578bd4"
                },
                "AU_21": {
                    "id": 20,
                    "name": "AU_21",
                    "color": "#578bd4"
                },
                "AU_22": {
                    "id": 21,
                    "name": "AU_22",
                    "color": "#578bd4"
                },
                "AU_23": {
                    "id": 22,
                    "name": "AU_23",
                    "color": "#578bd4"
                },
                "AU_24": {
                    "id": 23,
                    "name": "AU_24",
                    "color": "#578bd4"
                },
                "AU_25": {
                    "id": 24,
                    "name": "25",
                    "color": "#578bd4"
                },
                "AU_26": {
                    "id": 25,
                    "name": "26",
                    "color": "#578bd4"
                },
                "AU_27": {
                    "id": 26,
                    "name": "27",
                    "color": "#578bd4"
                },
                "AU_28": {
                    "id": 27,
                    "name": "28",
                    "color": "#578bd4"
                },
                "AU_29": {
                    "id": 28,
                    "name": "29",
                    "color": "#578bd4"
                },
                "AU_30": {
                    "id": 29,
                    "name": "30",
                    "color": "#578bd4"
                },
                "AU_31": {
                    "id": 30,
                    "name": "31",
                    "color": "#578bd4"
                },
                "AU_32": {
                    "id": 31,
                    "name": "32",
                    "color": "#578bd4"
                },
                "AU_33": {
                    "id": 32,
                    "name": "33",
                    "color": "#578bd4"
                },
                "AU_34": {
                    "id": 33,
                    "name": "34",
                    "color": "#578bd4"
                },
                "AU_35": {
                    "id": 34,
                    "name": "35",
                    "color": "#578bd4"
                },
                "AU_36": {
                    "id": 35,
                    "name": "36",
                    "color": "#578bd4"
                },
                "AU_37": {
                    "id": 36,
                    "name": "37",
                    "color": "#578bd4"
                },
                "AU_38": {
                    "id": 37,
                    "name": "38",
                    "color": "#578bd4"
                },
                "AU_39": {
                    "id": 38,
                    "name": "39",
                    "color": "#578bd4"
                },
                "AU_40": {
                    "id": 39,
                    "name": "40",
                    "color": "#578bd4"
                },
                "AU_41": {
                    "id": 40,
                    "name": "41",
                    "color": "#578bd4"
                },
                "AU_42": {
                    "id": 41,
                    "name": "42",
                    "color": "#578bd4"
                },
                "AU_43": {
                    "id": 42,
                    "name": "43",
                    "color": "#578bd4"
                },
                "AU_44": {
                    "id": 43,
                    "name": "44",
                    "color": "#578bd4"
                },
                "AU_45": {
                    "id": 44,
                    "name": "45",
                    "color": "#578bd4"
                },
                "AU_46": {
                    "id": 45,
                    "name": "46",
                    "color": "#578bd4"
                },
                "AU_01": {
                    "id": 1,
                    "name": "01",
                    "color": "#578bd4"
                },
                "AU_02": {
                    "id": 2,
                    "name": "02",
                    "color": "#578bd4"
                },
                "AU_04": {
                    "id": 3,
                    "name": "04",
                    "color": "#578bd4"
                },
                "AU_05": {
                    "id": 4,
                    "name": "05",
                    "color": "#578bd4"
                },
                "AU_06": {
                    "id": 5,
                    "name": "06",
                    "color": "#578bd4"
                },
                "AU_07": {
                    "id": 6,
                    "name": "07",
                    "color": "#578bd4"
                },
                "AU_08": {
                    "id": 7,
                    "name": "08",
                    "color": "#578bd4"
                },
                "AU_09": {
                    "id": 8,
                    "name": "09",
                    "color": "#578bd4"
                }
            }
        },
        "samples.subjects.actionUnits.headMovement": {
            "type": "multi_label",
            "categories": {
                "AU_51": {
                    "id": 46,
                    "name": "51",
                    "color": "#5781d4"
                },
                "AU_52": {
                    "id": 47,
                    "name": "52",
                    "color": "#5781d4"
                },
                "AU_53": {
                    "id": 48,
                    "name": "53",
                    "color": "#5781d4"
                },
                "AU_54": {
                    "id": 49,
                    "name": "54",
                    "color": "#5781d4"
                },
                "AU_55": {
                    "id": 50,
                    "name": "55",
                    "color": "#5781d4"
                },
                "AU_56": {
                    "id": 52,
                    "name": "56",
                    "color": "#5781d4"
                },
                "AU_57": {
                    "id": 54,
                    "name": "57",
                    "color": "#5781d4"
                },
                "AU_58": {
                    "id": 56,
                    "name": "M58",
                    "color": "#5781d4"
                },
                "AU_M55": {
                    "id": 51,
                    "name": "M55",
                    "color": "#5781d4"
                },
                "AU_M56": {
                    "id": 53,
                    "name": "M56",
                    "color": "#5781d4"
                },
                "AU_M57": {
                    "id": 55,
                    "name": "M57",
                    "color": "#5781d4"
                },
                "AU_M59": {
                    "id": 57,
                    "name": "M59",
                    "color": "#5781d4"
                },
                "AU_M60": {
                    "id": 58,
                    "name": "M60",
                    "color": "#5781d4"
                },
                "AU_M83": {
                    "id": 59,
                    "name": "M83",
                    "color": "#5781d4"
                }
            }
        },
        "samples.subjects.actionUnits.eyeMovement": {
            "type": "multi_label",
            "categories": {
                "AU_61": {
                    "id": 60,
                    "name": "61",
                    "color": "#5776d4"
                },
                "AU_62": {
                    "id": 62,
                    "name": "62",
                    "color": "#5776d4"
                },
                "AU_63": {
                    "id": 64,
                    "name": "63",
                    "color": "#5776d4"
                },
                "AU_64": {
                    "id": 65,
                    "name": "64",
                    "color": "#5776d4"
                },
                "AU_65": {
                    "id": 66,
                    "name": "65",
                    "color": "#5776d4"
                },
                "AU_66": {
                    "id": 67,
                    "name": "66",
                    "color": "#5776d4"
                },
                "AU_69": {
                    "id": 69,
                    "name": "69",
                    "color": "#5776d4"
                },
                "AU_M61": {
                    "id": 61,
                    "name": "M61",
                    "color": "#5776d4"
                },
                "AU_M62": {
                    "id": 63,
                    "name": "M62",
                    "color": "#5776d4"
                },
                "AU_M68": {
                    "id": 68,
                    "name": "M68",
                    "color": "#5776d4"
                },
                "AU_M69": {
                    "id": 70,
                    "name": "M69",
                    "color": "#5776d4"
                }
            }
        },
        "samples.subjects.actionUnits.visibility": {
            "type": "multi_label",
            "categories": {
                "AU_70": {
                    "id": 71,
                    "name": "70",
                    "color": "#576cd4"
                },
                "AU_71": {
                    "id": 72,
                    "name": "71",
                    "color": "#576cd4"
                },
                "AU_72": {
                    "id": 73,
                    "name": "72",
                    "color": "#576cd4"
                },
                "AU_73": {
                    "id": 74,
                    "name": "73",
                    "color": "#576cd4"
                },
                "AU_74": {
                    "id": 75,
                    "name": "74",
                    "color": "#576cd4"
                }
            }
        },
        "samples.subjects.actionUnits.grossBehavior": {
            "type": "multi_label",
            "categories": {
                "AU_50": {
                    "id": 76,
                    "name": "AU_50",
                    "color": "#5761d4"
                },
                "AU_80": {
                    "id": 77,
                    "name": "AU_80",
                    "color": "#5761d4"
                },
                "AU_81": {
                    "id": 78,
                    "name": "AU_81",
                    "color": "#5761d4"
                },
                "AU_82": {
                    "id": 79,
                    "name": "AU_82",
                    "color": "#5761d4"
                },
                "AU_84": {
                    "id": 80,
                    "name": "AU_84",
                    "color": "#5761d4"
                },
                "AU_85": {
                    "id": 81,
                    "name": "AU_85",
                    "color": "#5761d4"
                },
                "AU_91": {
                    "id": 82,
                    "name": "AU_91",
                    "color": "#5761d4"
                },
                "AU_92": {
                    "id": 83,
                    "name": "AU_92",
                    "color": "#5761d4"
                },
                "AU_97": {
                    "id": 84,
                    "name": "AU_97",
                    "color": "#5761d4"
                },
                "AU_98": {
                    "id": 85,
                    "name": "AU_98",
                    "color": "#5761d4"
                }
            }
        },
        "samples.subjects.actionUnits.*.phase": {
            "type": "single_label",
            "categories": {
                "onset": {
                    "id": 1,
                    "name": "onset",
                    "color": "#dbd8d5"
                },
                "apex": {
                    "id": 2,
                    "name": "apex",
                    "color": "#f27e0a"
                },
                "offset": {
                    "id": 3,
                    "name": "offset",
                    "color": "#8a4704"
                }
            }
        },
        "samples.subjects.actionUnits.*.intensity": {
            "type": "single_label",
            "categories": {
                "A": {
                    "id": 1,
                    "name": "A",
                    "color": "#f595aa"
                },
                "B": {
                    "id": 2,
                    "name": "B",
                    "color": "#f5738f"
                },
                "C": {
                    "id": 3,
                    "name": "C",
                    "color": "#f53d65"
                },
                "D": {
                    "id": 4,
                    "name": "D",
                    "color": "#f71e4d"
                },
                "E": {
                    "id": 5,
                    "name": "E",
                    "color": "#f5073b"
                }
            }
        },
        "samples.subjects.actionUnits.*.symmetry": {
            "type": "single_label",
            "categories": {
                "left": {
                    "id": 1,
                    "name": "left",
                    "color": "#FAFAFA"
                },
                "right": {
                    "id": 2,
                    "name": "right",
                    "color": "#EF4583"
                },
                "both": {
                    "id": 3,
                    "name": "both",
                    "color": "#5fde7b"
                }
            }
        },
        "samples.subjects.gender": {
            "type": "single_label",
            "categories": {
                "male": {
                    "id": 1,
                    "name": "male",
                    "color": "#FAFAFA"
                },
                "female": {
                    "id": 2,
                    "name": "female",
                    "color": "#EF4583"
                },
                "other": {
                    "id": 3,
                    "name": "other",
                    "color": "#5fde7b"
                }
            }
        },
        "samples.subjects.age.group": {
            "type": "single_label",
            "categories": {
                "AG_0_2": {
                    "id": 1,
                    "name": "AG_0_2",
                    "color": "#eb5f34"
                },
                "AG_3_9": {
                    "id": 2,
                    "name": "AG_3_9",
                    "color": "#ebb434"
                },
                "AG_10_19": {
                    "id": 3,
                    "name": "AG_10_19",
                    "color": "#7aeb34"
                },
                "AG_20_29": {
                    "id": 4,
                    "name": "AG_20_29",
                    "color": "#34eba2"
                },
                "AG_30_39": {
                    "id": 5,
                    "name": "AG_30_39",
                    "color": "#348ceb"
                },
                "AG_40_49": {
                    "id": 6,
                    "name": "AG_40_49",
                    "color": "#7434eb"
                },
                "AG_50_59": {
                    "id": 7,
                    "name": "AG_50_59",
                    "color": "#ba34eb"
                },
                "AG_60_69": {
                    "id": 8,
                    "name": "AG_60_69",
                    "color": "#eb34cc"
                },
                "AG_70_more": {
                    "id": 9,
                    "name": "AG_70_more",
                    "color": "#eb3474"
                }
            }
        },
        "samples.subjects.ethnicity": {
            "type": "single_label",
            "categories": {
                "white": {
                    "id": 1,
                    "name": "white",
                    "color": "#3a61b0"
                },
                "black": {
                    "id": 2,
                    "name": "black",
                    "color": "#b38022"
                },
                "asian": {
                    "id": 3,
                    "name": "asian",
                    "color": "#decd37"
                },
                "latino": {
                    "id": 4,
                    "name": "latino",
                    "color": "#faab61"
                },
                "indian": {
                    "id": 5,
                    "name": "indian",
                    "color": "#27753d"
                },
                "arabic": {
                    "id": 6,
                    "name": "arabic",
                    "color": "#41444a"
                },
                "middleEastern": {
                    "id": 7,
                    "name": "middleEastern",
                    "color": "#5f38ba"
                },
                "nativeAmerican": {
                    "id": 8,
                    "name": "nativeAmerican",
                    "color": "#de3737"
                },
                "pacificIslander": {
                    "id": 9,
                    "name": "pacificIslander",
                    "color": "#4fe0f0"
                },
                "other": {
                    "id": 10,
                    "name": "other",
                    "color": "#6d736e"
                }
            }
        },
        "samples.subjects.eyes.closed": {
            "type": "single_label",
            "categories": {
                "closed": {
                    "id": 1,
                    "name": "closed",
                    "color": "#eb5252"
                },
                "opened": {
                    "id": 2,
                    "name": "opened",
                    "color": "#53b874"
                },
                "halfOpened": {
                    "id": 3,
                    "name": "halfOpened",
                    "color": "#eded13"
                }
            }
        },
        "samples.subjects.transients.facialHair": {
            "type": "single_label",
            "categories": {
                "noBeard": {
                    "id": 1,
                    "name": "noBeard",
                    "color": "#eb5252"
                },
                "fullBeard": {
                    "id": 2,
                    "name": "fullBeard",
                    "color": "#53b874"
                },
                "mustache": {
                    "id": 3,
                    "name": "mustache",
                    "color": "#eded13"
                }
            }
        },
        "samples.subjects.transients.glasses": {
            "type": "single_label",
            "categories": {
                "noGlasses": {
                    "id": 1,
                    "name": "noGlasses",
                    "color": "#eb5252"
                },
                "correctionGlasses": {
                    "id": 2,
                    "name": "correctionGlasses",
                    "color": "#53b874"
                },
                "sunGlasses": {
                    "id": 3,
                    "name": "sunGlasses",
                    "color": "#eded13"
                }
            }
        },
        "samples.subjects.transients.hairStyle": {
            "type": "single_label",
            "categories": {
                "bald": {
                    "id": 1,
                    "name": "bald",
                    "color": "#eb5252"
                },
                "long": {
                    "id": 2,
                    "name": "long",
                    "color": "#53b874"
                },
                "short": {
                    "id": 3,
                    "name": "short",
                    "color": "#eded13"
                }
            }
        },
        "samples.subjects.transients.hairColor": {
            "type": "single_label",
            "categories": {
                "blond": {
                    "id": 1,
                    "name": "blond",
                    "color": "#edc513"
                },
                "brunette": {
                    "id": 2,
                    "name": "brunette",
                    "color": "#997253"
                },
                "red": {
                    "id": 3,
                    "name": "red",
                    "color": "#cc2d2d"
                },
                "black": {
                    "id": 4,
                    "name": "black",
                    "color": "#1a1818"
                },
                "white": {
                    "id": 5,
                    "name": "white",
                    "color": "#969393"
                },
                "other": {
                    "id": 6,
                    "name": "other",
                    "color": "#ffffff"
                }
            }
        },
        "samples.subjects.transients.skinType": {
            "type": "single_label",
            "categories": {
                "fair": {
                    "id": 1,
                    "name": "fair",
                    "color": "#e8e1e1"
                },
                "medium": {
                    "id": 2,
                    "name": "medium",
                    "color": "#948f8f"
                },
                "black": {
                    "id": 3,
                    "name": "black",
                    "color": "#1a1818"
                }
            }
        },
        "samples.subjects.transients.clothes": {
            "type": "multi_label",
            "categories": {
                "tShirt": {
                    "id": 1,
                    "name": "tShirt",
                    "color": "#878a22"
                },
                "pulloverJacket": {
                    "id": 2,
                    "name": "pulloverJacket",
                    "color": "#228a3f"
                },
                "thickWinterJacket": {
                    "id": 3,
                    "name": "thickWinterJacket",
                    "color": "#06080a"
                },
                "hat": {
                    "id": 4,
                    "name": "hat",
                    "color": "#22808a"
                },
                "baseballCap": {
                    "id": 5,
                    "name": "baseballCap",
                    "color": "#8a226b"
                },
                "hoodie": {
                    "id": 6,
                    "name": "hoodie",
                    "color": "#8a4f22"
                },
                "mask": {
                    "id": 7,
                    "name": "mask",
                    "color": "#13ebd9"
                }
            }
        },
        "samples.subjects.body.bodyType": {
            "type": "single_label",
            "categories": {
                "fullBody": {
                    "id": 1,
                    "name": "fullBody",
                    "color": "#d66920"
                },
                "upperBody": {
                    "id": 2,
                    "name": "upperBody",
                    "color": "#489923"
                },
                "lowerBody": {
                    "id": 3,
                    "name": "lowerBody",
                    "color": "#e013eb"
                }
            }
        },
        "samples.subjects.face.orientation": {
            "type": "single_label",
            "categories": {
                "fullLeftProfile": {
                    "id": 1,
                    "name": "fullLeftProfile",
                    "color": "#ebe534"
                },
                "halfLeftProfile": {
                    "id": 2,
                    "name": "halfLeftProfile",
                    "color": "#abeb34"
                },
                "straight": {
                    "id": 3,
                    "name": "straight",
                    "color": "#34ebc0"
                },
                "halfRightProfile": {
                    "id": 4,
                    "name": "halfRightProfile",
                    "color": "#3468eb"
                },
                "fullRightProfile": {
                    "id": 5,
                    "name": "fullRightProfile",
                    "color": "#c934eb"
                }
            }
        },
        "samples.subjects.eyes.orientation": {
            "type": "single_label",
            "categories": {
                "left": {
                    "id": 1,
                    "name": "left",
                    "color": "#ebe534"
                },
                "straight": {
                    "id": 2,
                    "name": "straight",
                    "color": "#34ebc0"
                },
                "right": {
                    "id": 3,
                    "name": "right",
                    "color": "#c934eb"
                }
            }
        },
        "samples.subjects.pain.AFF": {
            "type": "single_label",
            "categories": {
                "A": {
                    "id": 1,
                    "name": "A",
                    "color": "#f58282"
                },
                "B": {
                    "id": 2,
                    "name": "B",
                    "color": "#f57878"
                },
                "C": {
                    "id": 3,
                    "name": "C",
                    "color": "#f56e6e"
                },
                "D": {
                    "id": 4,
                    "name": "D",
                    "color": "#fa6666"
                },
                "E": {
                    "id": 5,
                    "name": "E",
                    "color": "#fc5b5b"
                },
                "F": {
                    "id": 6,
                    "name": "F",
                    "color": "#fa5555"
                },
                "G": {
                    "id": 7,
                    "name": "G",
                    "color": "#f74343"
                },
                "H": {
                    "id": 8,
                    "name": "H",
                    "color": "#f53b3b"
                },
                "I": {
                    "id": 9,
                    "name": "I",
                    "color": "#f23535"
                },
                "J": {
                    "id": 10,
                    "name": "J",
                    "color": "#f22e2e"
                },
                "K": {
                    "id": 11,
                    "name": "K",
                    "color": "#f52727"
                },
                "L": {
                    "id": 12,
                    "name": "L",
                    "color": "#f52020"
                },
                "M": {
                    "id": 13,
                    "name": "M",
                    "color": "#f51616"
                },
                "N": {
                    "id": 14,
                    "name": "N",
                    "color": "#f50c0c"
                },
                "O": {
                    "id": 15,
                    "name": "O",
                    "color": "#f70707"
                },
                "P": {
                    "id": 16,
                    "name": "P",
                    "color": "#fc0000"
                },
                "Q": {
                    "id": 17,
                    "name": "Q",
                    "color": "#ff0000"
                }
            }
        },
        "samples.subjects.pain.SEN": {
            "type": "single_label",
            "categories": {
                "A": {
                    "id": 1,
                    "name": "A",
                    "color": "#f58282"
                },
                "B": {
                    "id": 2,
                    "name": "B",
                    "color": "#f57878"
                },
                "C": {
                    "id": 3,
                    "name": "C",
                    "color": "#f56e6e"
                },
                "D": {
                    "id": 4,
                    "name": "D",
                    "color": "#fa6666"
                },
                "E": {
                    "id": 5,
                    "name": "E",
                    "color": "#fc5b5b"
                },
                "F": {
                    "id": 6,
                    "name": "F",
                    "color": "#fa5555"
                },
                "G": {
                    "id": 7,
                    "name": "G",
                    "color": "#f74343"
                },
                "H": {
                    "id": 8,
                    "name": "H",
                    "color": "#f53b3b"
                },
                "I": {
                    "id": 9,
                    "name": "I",
                    "color": "#f23535"
                },
                "J": {
                    "id": 10,
                    "name": "J",
                    "color": "#f22e2e"
                },
                "K": {
                    "id": 11,
                    "name": "K",
                    "color": "#f52727"
                },
                "L": {
                    "id": 12,
                    "name": "L",
                    "color": "#f52020"
                },
                "M": {
                    "id": 13,
                    "name": "M",
                    "color": "#f51616"
                },
                "N": {
                    "id": 14,
                    "name": "N",
                    "color": "#f50c0c"
                },
                "O": {
                    "id": 15,
                    "name": "O",
                    "color": "#f70707"
                },
                "P": {
                    "id": 16,
                    "name": "P",
                    "color": "#fc0000"
                },
                "Q": {
                    "id": 17,
                    "name": "Q",
                    "color": "#ff0000"
                }
            }
        },
        "samples.subjects.pain.OPR": {
            "type": "single_label",
            "categories": {
                "A": {
                    "id": 1,
                    "name": "A",
                    "color": "#f58282"
                },
                "B": {
                    "id": 2,
                    "name": "B",
                    "color": "#fa6666"
                },
                "C": {
                    "id": 3,
                    "name": "C",
                    "color": "#fa5555"
                },
                "D": {
                    "id": 4,
                    "name": "D",
                    "color": "#f23535"
                },
                "E": {
                    "id": 5,
                    "name": "E",
                    "color": "#f52020"
                },
                "F": {
                    "id": 6,
                    "name": "F",
                    "color": "#fc0000"
                }
            }
        },
        "samples.metadata.weather": {
            "type": "single_label",
            "categories": {
                "rainy": {
                    "id": 1,
                    "name": "rainy",
                    "color": "#4de342"
                },
                "sunny": {
                    "id": 2,
                    "name": "sunny",
                    "color": "#fffb00"
                },
                "cloudy": {
                    "id": 3,
                    "name": "cloudy",
                    "color": "#ccccc6"
                },
                "clear": {
                    "id": 4,
                    "name": "clear",
                    "color": "#05d8f0"
                },
                "snowy": {
                    "id": 5,
                    "name": "snowy",
                    "color": "#d0dadb"
                }
            }
        },
        "samples.metadata.dayTime": {
            "type": "single_label",
            "categories": {
                "morning": {
                    "id": 1,
                    "name": "morning",
                    "color": "#fa8100"
                },
                "afternoon": {
                    "id": 2,
                    "name": "afternoon",
                    "color": "#fffb00"
                },
                "night": {
                    "id": 3,
                    "name": "night",
                    "color": "#171716"
                }
            }
        },
        "samples.metadata.outdoorLight": {
            "type": "single_label",
            "categories": {
                "daytime": {
                    "id": 1,
                    "name": "daytime",
                    "color": "#171716"
                },
                "transitionOfLightning": {
                    "id": 2,
                    "name": "transitionOfLightning",
                    "color": "#171716"
                },
                "nightTime": {
                    "id": 3,
                    "name": "nightTime",
                    "color": "#171716"
                },
                "fullyDark": {
                    "id": 4,
                    "name": "fullyDark",
                    "color": "#171716"
                },
                "sunlightFalling": {
                    "id": 5,
                    "name": "sunlightFalling",
                    "color": "#171716"
                }
            }
        },
        "samples.metadata.indoorLight": {
            "type": "single_label",
            "categories": {}
        },
        "samples.metadata.colorSpace": {
            "type": "single_label",
            "categories": {
                "grayscale": {
                    "id": 1,
                    "name": "grayscale",
                    "color": "#9e9b9b"
                },
                "rgb": {
                    "id": 2,
                    "name": "rgb",
                    "color": "#ff0000"
                },
                "infrared": {
                    "id": 3,
                    "name": "infrared",
                    "color": "#9c6e6e"
                },
                "yuv": {
                    "id": 4,
                    "name": "yuv",
                    "color": "#8f3f3f"
                },
                "hsi": {
                    "id": 5,
                    "name": "hsi",
                    "color": "#911d1d"
                }
            }
        },
        "samples.subjects.eyes.*.label": {
            "type": "single_label",
            "categories": {
                "leftEye": {
                    "id": 1,
                    "name": "leftEye",
                    "color": ""
                },
                "rightEye": {
                    "id": 2,
                    "name": "rightEye",
                    "color": ""
                }
            }
        },
        "samples.subjects.hands.*.label": {
            "type": "single_label",
            "categories": {
                "leftHand": {
                    "id": 1,
                    "name": "leftHand",
                    "color": ""
                },
                "rightHand": {
                    "id": 2,
                    "name": "rightHand",
                    "color": ""
                }
            }
        },
        "samples.subjects.feet.*.label": {
            "type": "single_label",
            "categories": {
                "leftFoot": {
                    "id": 1,
                    "name": "leftFoot",
                    "color": ""
                },
                "rightFoot": {
                    "id": 2,
                    "name": "rightFoot",
                    "color": ""
                }
            }
        },
        "samples.subjects.**.landmarks*.region": {
            "type": "single_label",
            "categories": {
                "face": {
                    "id": 1,
                    "name": "face",
                    "color": ""
                },
                "body": {
                    "id": 2,
                    "name": "body",
                    "color": ""
                },
                "leftEye": {
                    "id": 3,
                    "name": "leftEye",
                    "color": ""
                },
                "rightEye": {
                    "id": 4,
                    "name": "rightEye",
                    "color": ""
                },
                "leftHand": {
                    "id": 5,
                    "name": "leftHand",
                    "color": ""
                },
                "rightHand": {
                    "id": 6,
                    "name": "rightHand",
                    "color": ""
                },
                "leftFoot": {
                    "id": 7,
                    "name": "leftFoot",
                    "color": ""
                },
                "rightFoot": {
                    "id": 8,
                    "name": "rightFoot",
                    "color": ""
                }
            }
        },
        "samples.**.boundingBox.region": {
            "type": "single_label",
            "categories": {
                "face": {
                    "id": 1,
                    "name": "face",
                    "color": ""
                },
                "body": {
                    "id": 2,
                    "name": "body",
                    "color": ""
                },
                "object": {
                    "id": 3,
                    "name": "object",
                    "color": ""
                },
                "leftHand": {
                    "id": 4,
                    "name": "leftHand",
                    "color": ""
                },
                "rightHand": {
                    "id": 5,
                    "name": "rightHand",
                    "color": ""
                },
                "leftFoot": {
                    "id": 6,
                    "name": "leftFoot",
                    "color": ""
                },
                "rightFoot": {
                    "id": 7,
                    "name": "rightFoot",
                    "color": ""
                }
            }
        },
        "samples.subjects.activity.handsOnWheel": {
            "type": "single_label",
            "categories": {
                "both": {
                    "id": 1,
                    "name": "both",
                    "color": ""
                },
                "left": {
                    "id": 2,
                    "name": "left",
                    "color": ""
                },
                "right": {
                    "id": 3,
                    "name": "right",
                    "color": ""
                },
                "none": {
                    "id": 4,
                    "name": "none",
                    "color": ""
                }
            }
        }
    }
