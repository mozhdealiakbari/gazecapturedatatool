from __future__ import annotations
import copy
import json
from typing import Tuple, List
import pandas
from ..data_validation.attribute import Attribute
from ..data_validation.containers.StorageValDict import StorageValDict
from .BaseTypes import CameraCalibration, ExperimentalType
from .ImageSample import ImageSample
from .VideoSample import VideoSample
from .GlobalAnnotations import GlobalAnnotations
from .base.BaseDTDataset import BaseDTDataset
from .AudioSample import AudioSample
import inspect
import os


class DTDataset(BaseDTDataset):
    imageSamples: StorageValDict[str, ImageSample] = Attribute(type=StorageValDict,
                                                               min_items=1,
                                                               element_constraints=Attribute(
                                                                   type=ImageSample,
                                                                   rsa=['id', 'samplePath'],
                                                                   osa=['subjects', 'objects']
                                                               )
                                                               )
    videoSamples: StorageValDict[str, VideoSample] = Attribute(type=StorageValDict,
                                                               min_items=1,
                                                               element_constraints=Attribute(
                                                                   type=VideoSample,
                                                                   rsa=['id', 'samplePath', 'intervals']
                                                               )
                                                               )
    audioSamples: StorageValDict[str, AudioSample] = Attribute(type=StorageValDict,
                                                               min_items=1,
                                                               element_constraints=Attribute(
                                                                   type=AudioSample,
                                                                   rsa=['id', 'samplePath'],
                                                                   osa=['transcription', 'keywords']
                                                               )
                                                               )
    cameraCalibrations: StorageValDict[str, CameraCalibration] = Attribute(type=StorageValDict,
                                                                           min_items=1,
                                                                           element_constraints=Attribute(
                                                                               type=CameraCalibration,
                                                                               rsa=['cameraId'],
                                                                               msa=2
                                                                           )
                                                                           )
    experimentals: StorageValDict[str, ExperimentalType] = Attribute(type=StorageValDict,
                                                                     min_items=1,
                                                                     element_constraints=Attribute(
                                                                         type=ExperimentalType,
                                                                         asa=True
                                                                     )
                                                                     )
    globalAnnotations: GlobalAnnotations = Attribute(type=GlobalAnnotations, msa=1)

    def __init__(self, name: str, operatingMode: str = 'memory'):
        args = locals()
        args.pop('self')
        BaseDTDataset.__init__(self, **args)

    def __del__(self):
        super().__del__()

    def to_pandas_frame(self, keep_original: bool = False, column_subset: List = None) -> List[Tuple[str,
                                                                                                     pandas.DataFrame]]:
        """
        Pandas dataframe generator for dataset

        :param keep_original: If original dataset needs to be kept in memory, if False, the original dataset object
        can be modified, by popping samples from it.
        :param column_subset: If only a subset of all columns are needed in the dataframe
        :return: List of (dataframe_name, pandas.Dataframe)
        """
        columns_subset = {}
        if column_subset is not None and type(column_subset) == list:
            for s in column_subset:
                columns_subset[s] = True

        class data_frame_creator:
            def __init__(self):
                self.subjects = {}
                self.subject_count = 0

            def add_subject(self, subject: dict):
                for k in self.subjects.keys():
                    self.subjects[k].append(subject.pop(k, None))
                for k, v in subject.items():
                    if (len(columns_subset) > 0 and k in columns_subset) or len(columns_subset) == 0:
                        self.subjects[k] = [None] * self.subject_count
                        self.subjects[k].append(v)
                self.subject_count += 1

            def get_dataframe(self):
                subjects_frame = None
                if len(self.subjects) > 0:
                    subjects_frame = pandas.DataFrame(self.subjects)
                return subjects_frame

        targets = [
            {
                'container': self.imageSamples,
                'frame_creators': [data_frame_creator(), data_frame_creator()],
                'frame_names': ['Image Samples Subjects', 'Image Samples Objects']
            },
            {
                'container': self.videoSamples,
                'frame_creators': [data_frame_creator(), data_frame_creator()],
                'frame_names': ['Video Samples Subjects', 'Video Samples Objects']
            },
            {
                'container': self.audioSamples,
                'frame_creators': [data_frame_creator()],
                'frame_names': ['Audio Samples']
            }
        ]

        for target in targets:
            sample_container = target['container']
            sample_keys = list(sample_container.keys())
            while len(sample_keys) > 0:
                key = sample_keys.pop()
                if keep_original is True:
                    sample = sample_container.get(key)
                else:
                    sample = sample_container.pop(key)
                if isinstance(sample, ImageSample):
                    base_info = {'sample.id': sample.id, 'sample.path': sample.samplePath}
                    if sample.metadata is not None:
                        sample.metadata.flatten(base_info, 'sample.metadata')
                    if len(sample.subjects) > 0:
                        for sub in sample.subjects:
                            out = copy.deepcopy(base_info)
                            sub.flatten(out, 'sample.subject')
                            target['frame_creators'][0].add_subject(out)
                    if len(sample.objects) > 0:
                        for obj in sample.objects:
                            out = copy.deepcopy(base_info)
                            obj.flatten(out, 'sample.object')
                            target['frame_creators'][1].add_subject(out)
                elif isinstance(sample, VideoSample):
                    for interval_id, interval in sample.intervals.items():
                        base_info = {'sample.id': sample.id, 'sample.path': sample.samplePath,
                                     'sample.interval.id': interval.id,
                                     'sample.interval.startFrame': interval.startFrame,
                                     'sample.interval.endFrame': interval.endFrame,
                                     'sample.interval.index': interval.index}
                        if sample.metadata is not None:
                            sample.metadata.flatten(base_info, 'sample.metadata')
                        if len(interval.subjects) > 0:
                            for sub in interval.subjects:
                                out = copy.deepcopy(base_info)
                                sub.flatten(out, 'sample.subject')
                                target['frame_creators'][0].add_subject(out)
                        if len(interval.objects) > 0:
                            for obj in interval.objects:
                                out = copy.deepcopy(base_info)
                                obj.flatten(out, 'sample.object')
                                target['frame_creators'][1].add_subject(out)
                elif isinstance(sample, AudioSample):
                    base_info = {'sample.id': sample.id,
                                 'sample.path': sample.samplePath
                                 }
                    if sample.transcription is not None:
                        base_info['sample.transcription'] = sample.transcription
                    if sample.metadata is not None:
                        sample.metadata.flatten(base_info, 'sample.metadata')
                    if len(sample.keywords) > 0:
                        for kw in sample.keywords:
                            out = copy.deepcopy(base_info)
                            kw.flatten(out, 'sample.keyword')
                            target['frame_creators'][0].add_subject(out)

            out = []
            for name, fc in zip(target['frame_names'], target['frame_creators']):
                if fc.subject_count > 0:
                    out.append((name, fc.get_dataframe()))
            return out

    def to_pandas_frame_for_report(self) -> List[Tuple[str, pandas.DataFrame, List[str]]]:
        """
        Get the pandas dataframe which is used for report generation. This method changes the column names to the
        expected names for columns in the report and only exports the columns which are required in the report.

        In addition it also export a column name list along with each dataframe holding the columns names which should
        be used to generate the interaction plots in the report.

        :return: List of (dataframe_name, pandas.Dataframe, List[columns_needed_for_interaction_plots])
        """
        cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        with open(os.path.join(cur_path, '../config/report_columns_mapping.json'), 'r') as r:
            report_columns_mapping = json.load(r)
        with open(os.path.join(cur_path, '../config/report_interaction_columns.json'), 'r') as r:
            report_interaction_columns = json.load(r)

        frame_out = self.to_pandas_frame(keep_original=False, column_subset=list(report_columns_mapping.keys()))
        out = []
        for tup in frame_out:
            tup[1].rename(columns=report_columns_mapping, inplace=True)
            out.append((tup[0], tup[1], report_interaction_columns))
        return out
