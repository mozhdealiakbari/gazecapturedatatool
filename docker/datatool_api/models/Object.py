from ..data_validation.base_model import BaseModel, Attribute
from .Categories import Categories
from .BaseTypes import BoundingBox, Cuboid, ScaleFactor, Segmentation


class Object(BaseModel):
    occluded: bool = Attribute(type=bool)
    label: Categories.Object = Attribute(type=Categories.Object)
    boundingBox: BoundingBox = Attribute(type=BoundingBox, rsa=['topX', 'topY', 'w', 'h', 'area'])
    cuboid: Cuboid = Attribute(type=Cuboid, rsa=['topX', 'topY', 'topZ', 'w', 'h', 'd', 'volume'])
    scaleFactor: ScaleFactor = Attribute(type=ScaleFactor, rsa=['value'])
    segmentation: Segmentation = Attribute(type=Segmentation, rsa=['segmentationType', 'value'])
    crowded: bool = Attribute(type=bool)

    def __init__(self, occluded: bool = None, label: Categories.Object = None, boundingBox: BoundingBox = None,
                 cuboid: Cuboid = None, scaleFactor: ScaleFactor = None, segmentation: Segmentation = None,
                 crowded: bool = None):
        args = locals()
        args.pop('self')
        super().__init__(**args)

    def add_annotation(self, annotation: BaseModel) -> None:
        """
        Add an annotation to the object

        :param annotation: The annotation to be added
        :return: None
        """
        if not isinstance(annotation, BaseModel):
            raise ValueError('Input annotation must be a sub-class of BaseModel')

        if type(annotation) == BoundingBox:
            setattr(self, 'boundingBox', annotation)
        elif type(annotation) == Segmentation:
            setattr(self, 'segmentation', annotation)
        elif type(annotation) == Cuboid:
            setattr(self, 'cuboid', annotation)
        elif type(annotation) == ScaleFactor:
            setattr(self, 'scaleFactor', annotation)
        else:
            raise ValueError('Annotation must belong to DTObject, check if you are adding the annotation at wrong place')
