from ..data_validation.base_model import BaseModel
from ..interfaces.StorageIOInterface import StoragePointer, StorageHandlerInterface


class MemoryStoragePointer(StoragePointer):
	__slots__ = ["obj"]
	
	def __init__(self, object_id, class_type, obj):
		super().__init__(object_id, class_type)
		self.obj = obj


class MemoryStorageHandler(StorageHandlerInterface):
	def write(self, object_id, data_category: str, data_object: BaseModel) -> MemoryStoragePointer:
		return MemoryStoragePointer(object_id, type(data_object), data_object)
	
	def read(self, pointer: MemoryStoragePointer, remove=False) -> BaseModel:
		return pointer.obj
	
	def close(self):
		pass

	def remove(self, pointer: MemoryStoragePointer) -> None:
		pointer.obj = None
