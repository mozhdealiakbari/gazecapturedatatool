#!/usr/bin/env bash

IMAGE_TAG=""
CONTAINER_NAME=""
PARAMS=""
OUTPUT_DIR=""
STORAGE=""
STORAGE_ROOT=""
NO_MOUNT=0

source common

function usage() {
  echo "Usage:

  source run.sh

  Required Arguments:
    --output-dir, -o            Directory where to dump the output of datatool


  Optional Arguments:
    --image-tag, -i             Tag of the image to run, if not specified, the image tag which was used to build the image is used
    --container-name, -c        Name to be given to the docker container, if not specified, default value is used
    --version, -v               Version of the datatool to run, this overrides the current version set in the datatool config
    --tags, -t                  List of tags to be processed (if not specified, all available tags for the datatool version are processed)


  Optional arguments for operations:
  --operation-mode, -om         Operation mode for the datatool, this overrides the default mode set in datatool config
  --validate-sample-paths, -vl  If sample paths must be validated while dumping the dataset json to disk, this overrides the default value set in datatool config


  Advance Optional Arguments:
    --storage, -s               Type of storage, [synology, s3 or local], this value overrides the default storage set in datatool config
    --storage-loc, -l           Address for remote storage (only used if storage is of type synology), this value overrides the location set in datatool config
    --storage-root, -r          Root directory holding the dataset:
                                For local storage: absolute path to the dataset root directory,
                                For synology storage: path to the synology root directory,
                                For s3 storage: bucket name of the bucket holding the dataset
    --no-mount, -n              Boolean flag, add it if you DO NOT WANT to MOUNT the output directory to docker and let docker write the output directly to it,
                                if this flag is specified, docker first creates the output internally and then output gets copied from docker to host
                                [This takes more space and time but more secure as docker never writes to the host directly]
  "
}

while (( $# )); do
  case "$1" in
    -h|--help)
      usage
      exit 0
      ;;
    -n|--no-mount)
      NO_MOUNT=1
      shift
      ;;
    -i|--image-tag)
      make_decision "$1" "$2" "image_tag"
      IMAGE_TAG="$2"
      shift 2
      ;;
    -c|--container-name)
      make_decision "$1" "$2" "container_name"
      CONTAINER_NAME="$2"
      shift 2
      ;;
    -o|--output-dir)
      make_decision "$1" "$2" "output"
      OUTPUT_DIR="$2"
      shift 2
      ;;
    -v|--version)
      make_decision "$1" "$2"
      shift 2
      ;;
    -om|--operation-mode)
      make_decision "$1" "$2"
      shift 2
      ;;
    -vl|--validate-sample-paths)
      make_decision "$1" "$2"
      shift 2
      ;;
    -s|--storage)
      make_decision "$1" "$2"
      STORAGE="$2"
      shift 2
      ;;
    -l|--storage-loc)
      make_decision "$1" "$2"
      shift 2
      ;;
    -r|--storage-root)
      make_decision "$1" "$2" "storage_root"
      STORAGE_ROOT="$2"
      shift 2
      ;;
    -t|--tags)
      tags=""
      while true; do
        if [ -n "$2" ] && [ "${2:0:1}" != "-" ]; then
          tags="$tags $2"
          shift 1
        else
          break
        fi
      done
      shift 1
      if [ -n "$tags" ]; then
        PARAMS="$PARAMS --tags $tags"
      fi
      ;;
    -*|--*=)
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
  esac
done

if [ -z "$OUTPUT_DIR" ]; then
  usage
  exit 1
fi

if [ -z "$IMAGE_TAG" ]; then
  IMAGE_TAG="$(getImageName)"
  if [ -z "$IMAGE_TAG" ]; then
    echo "Image tag to run the docker image can not be determined"
    exit 1
  fi
fi

if [ -z "$CONTAINER_NAME" ]; then
  CONTAINER_NAME="$(get_random_container_name)"
fi

MOUNT_INPUT=""
if [ "$STORAGE" = "local" ]; then
  if [ -z "$STORAGE_ROOT" ]; then
    echo "Error: [--storage-root, -r] must be provided in the arguments, if the storage is set to be local"
    exit 1
  else
    PARAMS="$PARAMS --storage-root /input"
    MOUNT_INPUT="-v '$(realpath "$STORAGE_ROOT"):/input:ro'"
  fi
else
  if [ -n "$STORAGE_ROOT" ]; then
    storage_path="$(realpath "$STORAGE_ROOT")"
    if [[ -d "$storage_path" ]]; then
      PARAMS="$PARAMS --storage-root /input"
      MOUNT_INPUT="-v '$(realpath "$STORAGE_ROOT"):/input:ro'"
    else
      PARAMS="$PARAMS --storage-root $STORAGE_ROOT"
    fi
  fi
fi

MOUNT_OUTPUT=""
if (( NO_MOUNT == 0 )); then
  MOUNT_OUTPUT="-v '$(realpath "$OUTPUT_DIR"):/output'"
fi

# When image is exported as tar -- uncomment next line to load the image
#docker load < "$IMAGE_TAG".tar

mkdir -p "$OUTPUT_DIR"

echo "Running the datatool"
command="docker run -t -d \
  -e DATATOOL_USER='$DATATOOL_USER' -e DATATOOL_PASSWORD='$DATATOOL_PASSWORD' \
  -e AWS_ACCESS_KEY='$AWS_ACCESS_KEY' -e AWS_SECRET_KEY='$AWS_SECRET_KEY' \
  $MOUNT_INPUT $MOUNT_OUTPUT \
  --name $CONTAINER_NAME $IMAGE_TAG"
eval "$command"

echo "Processing and creating the dataset"
docker exec -it "$CONTAINER_NAME" bash -c "python3 datatool.py --output-dir /output $PARAMS"

if (( NO_MOUNT == 1 )); then
  echo "Copying the data to host"
  docker cp "$CONTAINER_NAME":/output/. "$OUTPUT_DIR"
fi

echo "Removing the container"
docker rm -f "$CONTAINER_NAME"

echo "Process completed"
