# Visualize annotations on randomly drawn samples
This utility lets the user draw N samples at random from the datatool output and visualize the annotations on those randomly drawn samples.


## Installation: 

### Prerequisites
Ensure the system has the following dependencies: 
* Python 3.9 or higher

## Usage 

#### Virtual Environment (Optional)
It's strongly advised that you create [Python virtual environment](https://docs.python.org/3/library/venv.html) and install dependencies from [requirements.txt](requirements.txt) 
within the environment. This prevents risk of breaking existing system-wide packages. 

To create a virtual environment using Python's builtin `venv` module and activate:
```
python3 -m venv </path/to/new/virtual/environment>
source </path/to/new/virtual/environment>/bin/activate
```

#### Python Packages (Required)
To install the dependencies that are described in [requirements.txt](requirements.txt) inside the virtual environment:
```
pip3 install -r requirements.txt 
```

#### Visualize Annotations
Execute the `visualize_annotations.py` script to visualize the annotations on randomly drawn samples from datatool output
```
python3 visualize_annotations.py \
    --input-dir <INPUT_DIRECTORY> \
    --output-dir <OUTPUT_DIRECTORY> \
    --sample-count <SAMPLE_COUNT> \
    --operation-mode <OPERATION_MODE> \
    --random-seed <RANDOM_SEED>
```
Required arguments:
```
    --input-dir, -i        Directory containing the datatool output
    --output-dir, -o       Output directory where to save the random samples after adding the annotations to them
```

Optional arguments:
```
    --sample-count, -s      Number of random samples to draw, default is 100
    --operation-mode, -om   Operation mode for the datatool to load and process the json [memory, disk, ramdisk]
    --random-seed, -rs      Random seed value, provide it for reproducibility
```
